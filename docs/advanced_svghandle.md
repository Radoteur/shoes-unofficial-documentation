# Svghandle 

An svghandle is a non visual place holder into a svg (xml) file. Like an sqlite query handle is a temporary place inside the database. Every Svg widget has a handle (perhaps created by default).  You can do some manipulations
and creation of svghandles without an onscreen widget.

Many people can go merrily along their way without creating or manipulating svghandles.  But, if you want to or need to - you can create with them by calling 'app.svghandle' before creating any SVG widgets with them. Due to a lovely feature of Shoes you do need parens () around the {hash args}. Please examine the samples/good-cardflip.rb 
to see how to use svghandles.

## app.svghandle ({hash args})» svghandle 

Returns an object of SvgHandle class. 

First we'll discuss the hash args which must be combined in one call. Then we'll show the methods you can call on a svghandle.

Yes, parenthesis are required around the hash . Yes it is ugly.

## app.svhandle ( {filename: path} ) 

path is the pathname to an .svg file. If you are going to create multiple svghandles from one file (a deck of cards for example) then you really should read it into a ruby string and use the contents: below instead of reading the file multiple times.

## app.svhandle ( {contents: xmlstring} ) 

Creates an svghandle from xmlstring. More useful when you provide a group.

## app.svghandle( {group: string} ) 

Creates an svghandle for the given group name (the string must start with a '#')

## app.svghandle( {aspect: boolean or float} ) 

The aspect can be 'true` which is the the default. false has the effect of streching or shrinking it to the size of the Svg widget it will be shown in and float will show it in the given w/h ratio. 

## svghandle.width » a number 

Returns the internal svg handle (group?) width. Not very useful. 

## svghandle.height » a number 

Returns the internal svg handle (group?) height. Not very useful.

## svghandle.group? string » boolean 

Returns true or false (nil) if the svghandle has a group matching the string which starts with '#' »
