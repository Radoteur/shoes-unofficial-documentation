# Debugging

hoes has had some byebug capability since 3.2.25 but it was pretty difficult to use and pretty clunky. In Shoes 3.3.2 we have two ways to debug a script. Three ways in try the remote debugger but that adds nothing but confusion so pretend it doesn't exist. all of them require a command line launch so use cshoes.exe on Windows or create a cshoes.sh script in OSX via the Cobbler (maintain shoes from the splash gets to cobbler) or the Linux equivalent.

For example, given a `foo.rb` that looks like

```ruby
# check Shoes::VERSION constants. Not pretty
Shoes.app do
  stack do
    clist = []
    Shoes.constants.sort.each do |c|
     if c[/VERSION/] 
       t = "Shoes::#{c}"
       clist << "#{c} = #{eval t}"
     end
    end
    @eb = edit_box  clist.join("\n"), :width => 400, height: 400
    button 'quit' do
      Shoes.quit
    end
  end
end
```

Then start debugging with `cshoes.exe -d foo.rb` -- you will get a terminal that looks like

```ruby
[12, 21] in /home/ccoupe/Projects/shoes3/lib/shoes.rb
   12: else
   13:   # Normal shoes: 
   14:   # redefine quit and exit in Ruby to be Shoes implemented methods.
   15:   secret_exit_hook # maintains backwards compatibility
   16: end
=> 17: ARGV.delete_if { |x| x =~ /-psn_/ }
   18: 
   19: # Probably don't need this
   20: class Encoding
   21:   %w(UTF_7 UTF_16BE UTF_16LE UTF_32BE UTF_32LE).each do |enc|
(byebug) 
```

This is actually deep inside Shoes.rb. Where you may not want to be. You must set a breakpoint in your script and then continue Shoes until it gets to that breakpoint. Pretend we want to debug that Quit button behavior

```ruby
(byebug) b foo.rb:13
Successfully created breakpoint with id 1
(byebug) c
Stopped by breakpoint 1 at foo.rb:13

[8, 17] in foo.rb
    8:        clist << "#{c} = #{eval t}"
    9:      end
   10:     end
   11:     @eb = edit_box  clist.join("\n"), :width => 400, height: 400
   12:     button 'quit' do
=> 13:       Shoes.quit
   14:       #exit
   15:     end
   16:   end
   17: end
(byebug) 
```

Of course that break point won't be reached until you actually click the button. If you set the break point on line 12 then you're just debugging the creation of the button and not the result of a click on the button. At the breakpoint you can do all kinds of byebug things. Also see here

It is strongly suggested to not use byebugs 'irb'. If you do and it complains about exit and quit alias, you can get back to byebug with a ^D keystroke. There is special place in hell if you try 'irb' at the very first byebug prompt when it's deep in Shoes startup. Don't be that person - set a breakpoint and continue.

CAUTION: If your script uses exit or quit methods to terminate Shoes instead of the better Shoes.quit you may find that it doesn't work when debugging. Use the window close button and modify your script to call Shoes.quit.

The other way of debugging is to explicitly ask for byebug in your script and you make it conditional on the command line arguments. If you start your Shoes app with a double click then the only argument is the script name (or perhaps nothing in the args - OSX). That's good for normal. Since you want to debug and that requires a terminal/console then you can add a small amount of Ruby in your script and call it differently.

Add this at the very top of your main Shoes script (myscript.rb for this example)

```ruby
if ARGV.find_index('-d')
    ARGV.delete_if {|x| true}
    require 'byebug'
    byebug
end
Shoes.app do
# rest of script ....
end
```

If you launch with `cshoes.exe myscript.rb -- -d` then you'll get the byebug prompt and you can set a breakpoint and continue. If you launch from the desktop icon or `cshoes.exe myscript` then there is no -d arg possible. Unless you try really hard with Windows shortcuts or osx open --args which won't really work because it's not a terminal launch and byebug needs a terminal launch.
