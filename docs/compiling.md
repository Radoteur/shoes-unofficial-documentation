# Compiling

Once your app or prototype is done you may want to share you works and avoid the users to go through the installation of shoes. Two options are available for that :

| Method  | Pros  | Cons  |
|---|---|---|
| [Packaging](compiling_packaging.md) | - straightforward <br> - needs only one os for compiling any distination os | - Less configuration <br> - Shoes is very visible |
| [Merge packaging](compiling_merge_packaging.md#) | - Shoes is more hidden <br> - Create proper binaries (.dmg, .exe, .deb, .appimage)| - More complicated to setup <br> - You need to compile you app on the destination os |

Both method can be done via the gui and cli.

## Polishing Shoes
Do you want to make a Shoes app that doesn't look like a Shoes app? Maybe you just like to tweak things to scratch an itch?

Shoes 3.2 does have some facilities to make this slightly easier - primarily for Windows and OSX. It's not the easiest task you'll perform. You can't hide Shoes from someone who wants to poke around looking for it.

### Icons
Icons are used when packaging a Shoes app (replacing the Shoes icon). It's not a trivial process and some folks really really care about these things.

How hard is it to draw a picture? If you are graphically challenged (me) thats one problem. Then you have to create the right number of icon sizes in the correct formats and you have to bundle them. Did I mention you need Linux, OSX and Windows icons and a way on your system to do all three? 

Although some folks say you need really big icons (OSX retina) I'm going to stick with the lowest common denominator  256x256x32

I'm going to change the Shoes icon for 3.3 (Walkabout) below. Icons are bit map images not vector graphics and OSX thinks you need a 512 x 512 (x 32) so I'll start there. I found an image I like on the interwebs for the background, loaded it into Gimp, resized it to 512 px on the smaller dimensions did an elliptical selection of 512x512, crop, invert selection, add alpha channel and hit the delete key (clear background). Mind you this took many hours of failure. I get this:
![](http://shoes.mvmanila.com/public/images/walkabout-circle2.png)

Which is freakishly large for an icon but we'll resize later. Now I want to add the word "Shoes" to the icon in a tasteful Shoes-ful color with a font size of pretty big.
![](http://shoes.mvmanila.com/public/images/walkabout2-titled.png)

That doesn't completely suck, but my tolerance is high. Perhaps imagine an elliptical type base line that bows downward with some feathering and a drop shadow.  That might take a trip into Inkscape (svg editor for Linux) and back. One could also imagine, it's good enough for now.

We have our big 32 bit png. Let's convert it to Windows .ico and OSX .icns and make little pngs for Linux.

I used https://iconverticons.com/online/ to do the conversion for png. This particular icon sucks at small sizes, but they all do down there. The website created an .ico file that has 6 icons. For OSX make sure you **don't** download the hqx, you want the .icns. 

NOTE: the Linux `file walkabout.ico` command says it is 256 bit colors. **That is incorrect**. What's inside the file can be much different than what Linux reports.  They are all 32 bit icons at small sizes which Windows handles just fine. 

#### Windows
If you're reading this because you want a custom icon for the secondary or custom installer or you just want an icon for including in your app's directory so you can call `set_window_icon_path() then you've done enough. Stop reading. You have a png (your choice of size to put in the app dir) and you have an .ico if you want to drive a command line packaging script (you probably do want that - see those instructions is this section of the wiki)

If you are building your own Shoes or maintaining Shoes then changing the Shoes icon is, speaking politely, a bitch. You have to change the icons used by the NSIS installer (two+), the stub and you'll want to replace the installer background images and probably some other things. 

##### app.yaml
First you need to change app.yaml or replace the icon there (my choice because I'm not creating a custom Shoes).

##### stub32.rc
In platform/msw/ is stub32.rc which you have to edit or you can just do the chicken thing and copy your new icon over that location (shoes/appwin32.ico) I chose copy over it because I don't want to fix shoes/appwin32.rc too.

You absolutely must `rake stubs`

It's very possible that Windows will keep using some copies of the old icon. It has a cache and sometimes it needs to be told to rebuild. This can be a problem for the desktop Shoes.lnk. For Win 7 you can use a dos command `>ie4uinit.exe -ClearIconCache` which clears Windows icon cache.

##### NSIS bmp
The NSIS installer has two .bmp files it uses for the installer opening left side bar (installer-1.bmp) and it's progress dialog (installer-2.bmp). You need to edit those. Caution: Gimp defaults to exporting a bmp that the NSIS installer can't use. When you get the little dialog for bmp options you need to set the 'do not add colorspace" option and make sure the bit depth is 24. 

#### OSX

This is easy. Just use the .icns file for building Shoes or your custom package task.

#### Linux
Linux shares a icon cache update issue with Windows. Shoes 3.3 provides a desktop menu to Uninstall Shoes which does fix the problem so if you're changing the icon on Linux please install to get the menus and then `uninstall` gets the cache correct. Expect a few seconds delay on the install or uninstall.

### Gtk Themes for Windows (and Linux?)
Prior to Shoes 3.2.22, Shoes on Windows didn't use the built in theme engine. It just kind of defaulted to something reasonable. Now it uses the MS-Windows theme which is prettier. And you can tweak it! In fact, we had to tweak it just a tiny bit.

In gtk.c after the `gtk_init()` call is a commented call to `shoes_native_print_env` which lists the files Gtk is going to look for to get theme info. Uncomment and Shoes will report (cshoes.exe please) it's going to look for these

1. `{shoes_install_dir}\share\gtk-2.0\gtkrc`
2. `C:\PROGRAMDATA\gtk-2.0\gtkrc`
3. `{shoes_install_dir}\etc\gtk-2.0\gtkrc`
4. `C:{HOME}.gtkrc-2.0`

If there are multiple settings files, basically the last one wins. I strongly recommend you do not use #4, except for testing. Other Gtk programs like Gimp would see that settings file and you don't want that. Shoes puts it's tweak in #3. If you installed Shoes in the normal place that would be `C:\Program Files (x86)\Shoes\etc\gtk-2.0\gtkrc`. If you look at that you can see we set the font and size for widgets and friends.

If there are multiple settings files, basically the last one wins. I strongly recommend you do not use #4, except for testing. Other Gtk programs like Gimp would see that settings file and you don't want that. Shoes puts it's tweak in #3. If you installed Shoes in the normal place that would be `C:\Program Files (x86)\Shoes\etc\gtk-2.0\gtkrc`. If you look at that you can see we set the font and size for widgets and friends. 

[This page contains](http://gtk-win.sourceforge.net/home/index.php/Main/Downloads) some old but still good downloads. Pay attention to the details and warnings.  Download the Prefs Tools [supports 2.10 or later](http://downloads.sourceforge.net/gtk-win/gtk2_prefs-0.4.1.bin-gtk2.10-win32.zip?download). When you run that app you get an idea of what you can tweak.  Changes appear to be saved to #4 - did I warn you about that choice? Yes, I did. 

### Drive packaging with a script instead of clicks.

In Shoes 3.2, the packaging GUI and the code to do the work are in two separate files. You can can create a ruby script that sets the proper variables and call the second script. Which means that ruby script can build your app if you invoke shoes `$ cshoes --ruby myscript.rb`

### Your own website for download and packaging

In Shoes 3.2 the website is not hard coded into various binary programs that only windows or osx developers can fix. Don't like the current website? You can change Shoes to use a different website. The website will have to run a cgi which is currently a ruby script (1.8.7 I think) but it could be written in anything you like. 

### Shoes based custom installer.

Under development so we can't talk too much about it.
It doesn't  hide Shoes like the difficult process above but provides a way make you Shoes app appear to be an application - special launchers and icons and the like.
