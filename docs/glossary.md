# Glossary

- **Cobbler** : An internal tool of Shoes with many purposes, to access it click on `Maintain Shoes` on the splash screen.,
- **chsoes** : Name of the command line interface of Shoes explained [here](configuration.md#command-line).
- **Direwolf** : Name of the version 3.3.8 of Shoes.
- **Gempack** : A gempack is a gzipped tar ball of gems to put in end users `HOME/+gems`, an in depth explanation can be found on the [compiling page](compiling.md#gempack)
- **Loose Shoes** : Loose Shoes is one you built from source, mostly for advanced user and maintainers. 
- **Shy package** : A package containing you application as well as some informations like the version, creator and such. It is only mecessary to create a shy package when you want to [build your application using the packager](compiling.md#packaging)
- **Tight Shoes** : Downloaded version of Shoes (the only one must users needs), when you follow the [installation](getting_started.md#installation) page you are using thight Shoes.
- **Walkabout** : Name of the version 3.3.7 of Shoes
