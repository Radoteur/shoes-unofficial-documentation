# Monitors 

Shoes does know about multiple monitors and provides some ways you can use them. Of course, your system has to have multiple monitors and as a good author you'll want to check the users system before using them. 

See the Settings section of Advanced in this manual for more monitor information. 

You can specify the monitor for a Shoes app (window) using the sytle  `monitor: <int>` with the Shoes.app and/or window methods. 
 
 
You can get the monitor that a Window (app) is using with the `app.monitor` method and you can move a window to a different monitor with `app.monitor = ` method

```ruby
Shoes.app do
  para "This app is on monitor #{app.monitor}"
end
```
