# Structuring 

Using muliple ruby/shoes files to build an application is one way to group
code.

## Url and visit 

This is a mostly unknown way to structure a larger Shoes application. Lets say you
want three windows, a startup window, a data entry window and a help window. 
You subclass Shoes and use the url command to define the window names.
```ruby
class MyApp < Shoes
	url "/", :setupscreen
	url "/entry", :entryscreen
	url "/help", :helpscreen
  
  def setupscreen
    stack do 
      para "Welcome to My Demo app"
      flow do
        button "entry" do visit '/entry'end
        button "help" do visit '/help' end
      end
    end
  end
  
  def entryscreen
    stack do 
      para "Entry Screen - whats your secret?"
      @the_secret = edit_line text: "Secret"
      flow do
        button "home" do visit '/'end
        button "help" do visit '/help' end
      end
    end
  end
  
  def helpscreen
     stack do 
      para "This page describes MyApp, a very demo for structuring a Shoes application "
      flow do
        button "entry" do visit '/entry'end
        button "home" do visit '/' end
      end
    end end
end
Shoes.app :width => 400, :height => 300, :margin => 5
```

That one you can run from the manual. It's also in samples/expert-url.rb

Is the @the_secret.text available in the other screens? No? How would you
find it? What if each screen did a `require "./helper_methods"` or something
that added methods to Module.Shoes ? 

There's no `one` proper way that I know of and probably six ways to Sunday to do it.

There is a limit on how far you are willing to push Shoes when you start down this path.

Remember to have fun!
