# Furthermore 
 Run the samples from here, or read more Shoes information on the Internet. 

## Sample Apps 

In the Shoes directories are these samples you can run from the manual. This code for those samples are available on the [github project of shoes](https://github.com/shoes/shoes3/tree/master/samples)

Note: Cobbler (Maintain Shoes) has an option to copy the samples to a directory of your choice so you can modify them as you like and you won't have to poke around inside Shoes directories and change something you didn't want to change. 


## Internet Resources 

Shoes has many Ruby related features that haven't been described here. How can I learn more?

The opening splash screen hints at the secrets. Packaging and Maintaining? What's that? And what do all the options and buttons mean when I do run them? Relax, you  should know that these resources exist. If you're just beginning, feel free  to click around but it can be a firehose of information. You don't need to know it to have fun with Shoes. Just know it exists and you can come back here when your ready to inhale. 


See the [Shoes 3.2 Wiki](https://github.com/Shoes3/shoes3/wiki) and learn about Gems and packaging and much more. Find reference links to learning Ruby and more inside Shoes details.

 * [Shoes 3.2 source code.](https://github.com/Shoes3/shoes3)
 * You should join the [Shoes mailing list ](http://lists.mvmanila.com/listinfo.cgi/shoes-mvmanila.com)
 * Bookmark the [Shoes 3.2 blog](http://walkabout.mvmanila.com/) or the [RSS feed](http://walkabout.mvmanila.com/feed/)
 * Early [Downloads](http://shoes.mvmanila.com/public/shoes/) (work in progress) that may not be announced 

Older Shoes Links for historical purposes (may be dead):

 * [Old Shoes ML](http://librelist.com/browser/shoes/)
 * [Shoes 3.1 Source Code](http://github.com/shoes/shoes/) is on GitHub.
 * [Recent Builds](http://github.com/shoes/shoes/downloads) for your platform.

## Shortcuts 

Shoes provides the following general keyboard shortcuts:

 * `Alt-/` or `⌘-/` opens **Shoes Console**.
 * `Alt-.` or `⌘-.` opens **Open File…** dialog.
 * `Alt-?` or `⌘-?` opens **Shoes Manual**.
 * `Alt-=` or `⌘-=` opens IRB **Shoes Interactive Ruby**.
 * `Alt-;` or `⌘-;` opens **Remote byebug server**.

Shoes Manual provides the following keyboard shortcut:

 * `Alt-f` or `⌘-f` opens **Search** section.
 * `Alt-left` or `⌘-left` browses back.
 * `Alt-right` or `⌘-right` browses forward.
 * `PgUp` and `PgDn` scrolls up and down one page.
 * `Up` and `Down` scrolls up and down one line.

Shoes Console provides the following keyboard shortcut:

 * `Alt-t` or `⌘-t` disables autoscroll.

## IRB Interactive Ruby 

Shoes provides an **Interactive Ruby** (IRB) console for quick expression evaluations, fast prototyping and manipulate Shoes objects.

Press `Alt-=` or `⌘-=` to open an Interactive Ruby console.

### Getting started 

IRB comes with embedded commands to facilitate your interaction. Type **help** in your current session to list all available commands. 

![irb in shoes](img/furthermore_irb-help.png)

[Ruby in Twenty Minutes](http://www.ruby-lang.org/en/documentation/quickstart/) is an excellent tutorial on Ruby and the first 3 pages are focused on IRB. You can type in those examples in your Shoes Interactive Ruby console.

### Interacting with Shoes 

Now let's interact with Shoes objects in order to incrementally create the following GUI in a fast prototyping fashion. 

![prototype result](img/furthermore_prototype-result.png)

Type the following code in your IRB console to create a [Shoes.app](introduction.md#shoesappstyles-shoesapp) with a specific title and size. The variable **app** will contain your newly created window but it is possible to search Shoes apps amongst [Shoes.APPS](introduction.md#shoesapps-an-array-of-shoesapp-objects). 

`app = Shoes.app(:title => "History Lesson", :height => 75, :width => 300) {}`

A stack is added to contain the text that will display the historical era. You will notice that simply calling [para](slots.md#paratext-shoespara) in IRB will result in a NoMethodError exception. Para is essentially a method that belongs to a [Slots](slots.md), hence **app.para** is used here.

`app.stack { @p = app.para }`

The following creates a [flow](slots.md#flowstyles-shoesflow) allowing buttons to align side-by-side and also the first button. The button is clickable immediately after typing the code below.

`f = app.flow { app.button("1800s") { @p.replace "1800s" } }`

It is possible to manipulate elements in any [Slots](slots.md). Let's add another historical era to your your flow.

`f.button("1900s") { @p.replace "1900s" }`

Shall we verify that those buttons are indeed part of the flow **f**?

`f.contents`

Add another button to fill up some space.

`f.button("2000s") { @p.replace "2000s" }`

You can list your command history and see what you have done so far. It is also possible to save the command history to a file by typing **save**. For now, let's just take a look at the command **history**.

`history`

Finally, add a last button for entertainment value. You can press **up** or **down** to navigate through your history until you find the previous button, press **left** or **right** to edit the values, and press **enter**.

`f.button("2100s") { @p.replace "2100s" }`

Your **Interactive Ruby** console should look like this: 

![prototype console](img/furthermore_prototype-console.png)
