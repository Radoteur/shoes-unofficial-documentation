# Terminal 

Sometimes you want a logging window or even a window you can read keystrokes from like an old school terminal. Shoes does have one. It's not very clever and most people don't need it. Still, it has its uses.  The name changed in Shoes 3.3.2 to avoid confusion with all the existing uses of the word 'console' which usually mean the log window (alt-/). 

Windows users can't use any of the tailoring options (they are silently ignored) so  it's kind of cross platform and Windows does create a new 'console' and it works within the limitations of MSFT's definition of a terminal. 

## Shoes::show_console 

That is the old (deprecated) way. It just calls Shoes.terminal with defaults.

## Shoes.terminal {hash args} » nothing useful 

Will bring up a new window that hooks stdin, stdout and stderr  so it displays that output in the new window. This is a very brutal thing to do to the perfectly happy C `printf()` and Ruby `puts` that don't normally display. 

The terminal window cannot be controlled by other Shoes method calls other than its creation. There can be only one terminal because there is only one stdout, stderr, and  stdin. 

Note: Terminals are not cross platform so what works on Linux may not work on  Windows.  Windows is pretty dumb about consoles although I've heard Win 10 is much better.

Windows also behaves differently if you launch your Shoes script that calls Shoes.terminal from the commandline - it uses that launch 'console' for display. If you launch a Shoes script that calls Shoes.terminal from the menus/shortcuts then it will create a  new dos console window. It's not really a problem that needs solving.

You can modify what is printed on the Shoes.terminal by embedding escape sequences in the string you write to stdout or stderr. Shoes Linux and Shoes OSX pretend to be xterm and xterm-256color devices and support a subset of color and  character highlighting. Mostly to accomodate gems like test/unit. Windows is not an xterm so those escape sequence will fail unless the end user has installed one of those ANSI.dll things or Windows escape sequences are sent from the  script or gem. NOT Something Shoes can fix. 

Those optional hash args to configure the terminal are described below. They have to be combined into one hash arg for the one any only call you  can make. You can call Shoes.terminal as many times as you like but only 
the first call sticks.

Note: on Shoes/OSX ruby's stdout.tty? may return nil depending on lauch method.  Your gem (eg" test/unit) may need additional arguments {use_color: true}. Another problem Shoes can fix.

## Shoes.terminal {title: "string"} 

defaults to "Shoes Terminal"

## Shoes.terminal {columns: integer, rows: integer} 

number of characters across and lines down. 80 x 24 is the default.

## Shoes.terminal {fontsize: integer} 

default is 10 for Linux and 12 for OSX. Point size.

## Shoes.terminal {fg: "color", bg: "color"} 

default is "black" text foreground (fg) on a "white" background (bg).

They do have to be strings. Although Linux has a long list of color names available, OSX only has "black", "red", "green", "brown", "blue", "magenta","cyan","white" and "yellow" 

As mentioned above none of these args matter to Windows.  Choose wisely if you're going to set these colors. "Choose wisely" is kind of like " just don't". 

## CommandLine 

For folks that like to use their terminal or Windows console to run Shoes there is a pretty rich and useful set of options if you know where the Shoes program is. Where it is actually a bit tricky so we may need to create an command for it which we call `cshoes` 

```shell
$ cshoes -h
Usage: shoes [options] (app.rb or app.shy)
    -d, --debug                      Debug Shoes script
    -m, --manual                     Open the built-in manual.
    -w, --console                    display console
        --old-package                (Obsolete) Package a Shoes app for Windows, OS X and Linux.
    -c, --cobbler                    Maintain Shoes installation
    -p, --package                    Package Shoes App (new)
    -g, --gem                        Passes commands to RubyGems.
        --manual-html DIRECTORY      Saves the manual to a directory as HTML.
        --install MODE SRC DEST      Installs a file.
        --nolayered                  No WS_EX_LAYERED style option.
    -v, --version                    Display the version info.
    -h, --help                       Show this message
```

You can't run that from this manual. Some of those command switches are obvious.  Don't expect --manual-html, --install or --nolayered to work.

There is a another  option that can be really useful for some folks. `--ruby rubyargments` This one uses the ruby inside Shoes to execute the  `rubyargments` One of those arguments would be a script to execute with  Shoes ruby. One can package apps this way. See the wiki for details.

Likewise -g manipulates gems inside the Shoes location. `-g env` for example or `-g install myoddball-1.0`. Be careful - with great power comes great responsibility.

## cshoes.exe (Windows) 

Shoes on Windows has shoes.exe and cshoes.exe. If you want to use the Windows Console for development or testing, please use cshoes.exe.

## ./cshoes (OSX) 

Life is not so easy on OSX. You have to use Maintain Shoes (cobbler) to create a bash script that almost does what you want. 

## cshoes (Linux) 

Linux doesn't have a ./cshoes but you can create one if you're willing to modify .bashrc/.bash_profile or what ever shell you're using. For example

`alias cshoes='~/.shoes/walkabout/shoes'` 
