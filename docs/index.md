# Shoes 3 unofficial documentation
![logo](img/shoes_logo.svg)

## What is Shoes ?

Shoes 3 is a [open source](https://github.com/shoes/shoes3) graphical app kit for [Ruby](https://www.ruby-lang.org/en/) created by [_why](https://en.wikipedia.org/wiki/Why_the_lucky_stiff) and currently maintained by [Cecil Coupe](http://walkabout.mvmanila.com/my-history-of-shoes/) and other [contributors](https://github.com/shoes/shoes3/graphs/contributors).

Shoes is a tiny graphics toolkitck. It's simple and straightforward. Shoes was
born to be easy! Really, it was made for absolute beginners. There's really
nothing to it.

You see, the trivial Shoes program can be just one line:

```ruby
 Shoes.app { button("Click me!") { alert("Good job.") } }
```

Shoes programs are written in a language called Ruby. When Shoes is handed
this simple line of Ruby code, a window appears with a button inside reading
"Click me!" When the button is clicked, a message pops up.

On Linux, here's how this might look:

![simple app in linux](img/index_simple-app.png)

While lots of Shoes apps are graphical games and art programs, you can also layout text and edit controls easily.

![multiple apps in osx](img/index_multiple-apps.png)

Currently Shoes is running for the following plateforms :

- Linux
- Microsoft Windows XP to 10
- Mac OSX
- Raspberry pi
- BSD

## About this website

This website is a **non official** documentation for Shoes 3, most of the content come from the official manual available inside Shoes itself as well as the [official wiki](https://github.com/shoes/shoes3/wiki). 

This documentation intend to contain all the informations needed to create an app using Shoes 3 but will not contain anything about Shoes 4 or to much details on the guts of Shoes. See that as the entry door for begginner. If you want more information about how to help to develop Shoes or if you want to understand how it's workinng then the wiki or the [developement blog](http://walkabout.mvmanila.com/) are where you want to go.

The informations on this documentation aim to be as up to date as possible.

## Additional documentation
You should read [Nobody Knows Shoes](http://cloud.github.com/downloads/shoes/shoes/nks.pdf).
because it's funky and artful and it'll give you a taste of what can be done
with Shoes.

Have a question? Just want to talk Shoes? -  You really should join the Shoes [mailing list](http://lists.mvmanila.com/listinfo.cgi/shoes-mvmanila.com) and ask away. That's also a good place to discuss bugs if you don't want to get a github account. If you'd like to be a little more active about what's
going on with Shoes 3 you should [read the blog](http://walkabout.mvmanila.com/).

I also encourage you to read the known bug reports at [Shoes3 github](https://github.com/Shoes3/shoes3).
You'll need a free github account if you want to post there.

### New to ruby ? 
Here are some good sources to learn the language : 

- [Ruby in twenty minutes](https://www.ruby-lang.org/en/documentation/quickstart/)
- [The definitive ruby tutorial for complete beginners](https://www.rubyguides.com/ruby-tutorial/)
- [Programming Ruby - the pragmatic programmer's guide](http://ruby-doc.com/docs/ProgrammingRuby/)
