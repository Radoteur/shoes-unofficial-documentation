# Settings 

Shoes has a Settings class that is populated at startup with some values that are common to all Windows/Apps. The values in settings are created before Shoes starts your script - even before Shoes starts it's own scripts.

You can supply a startup.yaml file with your application and Shoes will use those instead of the normal defaults. This should interest those that want to create more  polished applications that the simple Shoes defaults. Please see the wiki for some additional hints. Most of these settings are to improve Merge packaging. 
Again, see the Wiki. 

## Shoes.settings » object 

Once you get the settings object then you check some things. Some you can modify. 

## settings.app_name » string 

Returns the application name. By default this is "Shoes" and is used as the  default to Winodws titles and dialog titles and a few other user visible places.

## settings.app_name = string 

Set the default application name. The string will be used where ever "Shoes" would appear in the menus or titles. 

## settings.icon_path » string 

Returns the Icon_Path string from the startup.yaml. The icon is a png that should be inside the Shoes (your) app. Default is "static/shoes-app-icon.png"

## settings.icon_path  = path_string 

When called this way it's behaves like `app.set_window_icon_path`

## setting.theme » string 

For Linux and Windows, you can include a Gtk3 theme name in the startup.yaml file, field `Theme;` The theme must be included with Shoes (see Merge packaging) in a themes folder. If given, it's checked and used before any theme choices in the user's diretory - `~/.shoes/themes/` or `{APPDATA}/Local/Shoes/Themes`, should the user have them.
On linux, the theme will override/merge with the theme chosen in your desktop window manager. 

## settings.extra1 » string 

Returns the `Extra1` string from the startup.yaml. Up to the developer what the string means or does.

## settings.extra2 » string 

Returns the `Extra2` string from the startup.yaml. Up to the developer what the string means or does.

## setting.use_menus » boolean 

Returns the setting of the startup.yaml `Use_menus` entry. This will put a small default menubar on every window (app)  in the application. If used, it overrides the individual `menus: ` value for app/windows creation. 

## settings.monitor_count » integer 

Returns the number of monitors attached to the system. There will always be one (1) monitor.

## settings.monitor_default » integer 

Returns the default monitor number. This is the one with the dock or global menus. What the number is depends on how the user setup his monitors. 

Note: The user can also change his monitor setup so you need to be clever when using multiple monitors in Shoes. 

## settings.monitor_geometry(<index>) » integer 

Returns the geometery of the monitor given the index. By definition this is a user specific value and it's also platfrom dependent. It is not wise to use the x,y values unless you are targeting have a very specific single plaform, single use application. In particular, try not use the second monitor for full screen pixel based drawing (games).

## settings.mdi » boolean 

A Linux and Windows Future. Not implemented yet. OSX already does this and needs no setting. MDI is Multi-Mocument-Interface. When `MDI: true` is in the startup.yaml file the the desktop window manager will send `open` requests to
a running Shoes app when the user double clicks on a file the desktop manager believes your application will handle. Linux uses the registered `dbus` name, See below.

May not be implemented. 

## settings.rdomain » string 

If an `Rdomain: <string>` entry is in the startup.yaml file then it will be used  instead of `com.shoesrb` to form the dbus name. Your setting for `App_Name` will be used as well. So the default dbus name would be com.rbshoes.shoes. If MDI is false then the  process id (pid) in Linux is appended. That make the dbus name fairly unique  (enough) that you can start multiple shoes apps without one of them grabbing open requests from the desktop that don't belong to it.  When MDI is true,  you don't  want a unique name.  This has effects for systray notifications. 

## settings.dbus » string 

Linux and Windows only. As described above, this returns the registered dbus name for the process. Linux users might use that and a dbus gem to communicate with other applications that support dbus. Many Linux application do talk to dbugs and in theory your, Shoes application can too. In Theory.

For example
```ruby
Shoes.app do
  stack do
    st = Shoes.settings
    para "You have #{st.monitor_count} monitors."
    para "The default monitor is #{st.monitor_default}"
    st.monitor_count.times do |mon|
      para "Monitor #{mon} => #{st.monitor_geometry mon}"
    end
  end
end
```

## settings.wintmo » integer 

Only usable for Windows and only until the problem is fixed. See next paragraph. Returns the current setting.

## settings.wintmo = integer 

Only for the Windows platform. Integer is the number of milliseconds for the mainloop timeout (tmo). This controls how frequently time is given to backgound Ruby threads. The default value is 10 which is a good balance of responsiveness for things like animating a progress bar with a download - download use threads. If you have a long operation like a very large copy of a complex file tree operation that is run in another thread then it may not get as much time as you want. In that case try setting the tmo to 2 or even 1. 

It Is Highly Recommended that you restore the value after your long threaded operation. It's meaning  and operation may seem counter intuitive. You should only do this if you really have a long threaded  background operation (aka minutes long). Lower values will reduce the GUI speed.  Setting to zero would be a very silly thing to do, so don't do it. Don't pass in floating point numbers.

Generally speaking, don't use this call. There is no setting in the startup.yaml file for this.

## settings.display_backend » string 

For Linux, this is the value from the startup.yaml `Display_Backend` field. It contains the names , comma separated, of the backends. 'x11' is the default. Unless you know better, this field should not be set and 'wayland' is the only value that works. If you have a wayland backend. 


## settings Image_Cache 

The Image_Cache: setting in startup.yaml does not have a Settings class method. You can use the app.methods to get or change the value. The only value is  `Image_Cache: false` to turn off the default.

## settings OSX_Menu_Trim 

This only exists in startup.yaml. There is no method to query the setting. `OSX_Menu_Trim: true` in startup.yaml will not display the Shoes entries of Open, Manual, Cobbler, Package and Profile" in the global menubar for OSX. This might be useful when your packaged app doesn't want to to show all that could be done.
