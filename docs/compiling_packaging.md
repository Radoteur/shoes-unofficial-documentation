# Packaging
User packaging is done using Shoes. Nothing more than a working Shoes (of any type) is needed. There are no rakefiles, no source code, no arcane editing mysterious files or yaml required. It's all about sharing a Shoes script or app you've written. You want to make it easy for for others to use your code.

Note: When you package a script or directory for a platform, you really should test the resulting package on a machine (Real or Virtual) that doesn't have Shoes installed on it. Get some friends to help out before you just dump it on the internet. That's what real programmers do.

Another Note: Packaging is actively being debugged, modified, screwed up, fixed and improved. If your screen has an advance button or check box and you select it, proceed with caution and low expectations.

![packaging screen in shoes](img/compiling_packager.png)

## Create shy for your app
If your app is a directory or if you want to create a package using advanced install options (like custom icon) you will need to creat a .shy first. 

Shy is just a container like tar or zip. It's actually a snip of yaml in front of the tar data and then zipped. When Shoes needs to run a .shy it unzips and untars it in a temp directory and runs the starting script you told it too. The shy dialog box is pretty simple -- truth is, you just have to enter a few characters in each edit_line. and click a few buttons to build the shy.

To see the shy creation screen click on `Bundle an App (shy)` when opening Shoes and select your main script. 
Fill all the fields and click on `Build .shy`. You now can create the package for your app.

## Create the package
To see the packaging screen click on `Package an App with Shoes` when opening Shoes than follow these steps to create your package : 

### 1. Select your script
Select the Shoes script that you created, if your app is bigger than just a file don't forget to click on the  `directory` link and to choose the main file of your app.

### 2. Choose is you want to include Shoes in your package
You can create a package that does not contain a version of Shoes, which means that when a user will install your program Shoes will be dowloaded and installed on their computer. 
If you choose to include Shoes your package will be a bit heavier.

### 3. Choose if you want the advance installer
- Custom install script
- Expand shy in user directory
- [Gempack](advanced_gempacks.md)
- Icon

### 4. Choose architecture
In this section you can choose the architecture you want to target for this packages, the following architectures are available : 

- Win 32 : Windows (all versions)
- Linux_Raspberry : Raspberry pi
- Linux_i686 : Linux 32 bit
- Linux_x86-64 : Linux 64 bit
- OSX : Mac OSX
- FreeBSD

When clicking on the name of an architecture the files needed for this architecture will be downloaded, if you already downloaded them before Shoes will informs you and ask you if you want to use the files you already downloaded or download them again.

Cliking on the architecture name will also create the package you want.

The package should be in the same directory as your main script.

Note: this is subject to change. - adding new options or changing the names or new yaml fields. This note is based on Shoes 3.3.1 r2353 (Feb 5, 2016)

## Packaging from the command line

Packaging from the GUI involves lots of button clicks and testing and redoing. It's a bit of a pain if you want to automate your development. You can create a couple of small Ruby scripts that you run from the command line using the --ruby option (`cshoes.exe --ruby scriptname.rb` for the Windows brethren) 

### Shy files

You probably want to create a .shy unless your project is exactly one .rb file. A .shy is bit of magic binary numbers followed by some yaml followed by a tar ball. The yaml has fields. Here's and example of a script that creates a shy.
```ruby
require 'shoes/packshoes'
shydesc = {}
shyfile = File.join("/","tmp", "shy", "ytm.shy")
shydesc['name'] = 'Yield to Maturity'
shydesc['version'] = '0.123'
shydesc['creator'] = 'the mighty OZ'
shydesc['launch'] = 'ytm.rb'
PackShoes.make_shy(shyfile, '/home/ccoupe/Projects/ytm', shydesc)
```
#### shyfile

This is the file to create. In the example, it's /tmp/shy/ytm.shy. Do not put it in the same directory or a subdirectory of that your shy-ing up. Don't do that unless you have infinite time and disk space. You don't have either. 
 
#### shydesc

The name, version and creator values are pretty much ignored (that won't be true in future Shoes) so pick something that your users will understand if the see it displayed). 

launch is the startup script in the directory that Shoes will run when the shy is executed on their system.

#### make_shy

The arguments should be apparent. The first is the absolute path of the .shy to create. The second argument is the directory/folder to package up into the .shy and the third argument is the desc hash.

### Packaging

Now, I'm going to describe the script you write to drive packaging. After you created a .shy file if that's what your app needs. The 'test.rb' below just has one ruby script. Shoes only packs one file - a .rb or a .shy.

What's in the test.rb? Basically you require the packshoes.rb, fill out an platform specific opts hash and
call one of the methods in PackShoes. There are many caveats, bewares and huh? . Please study lib/shoes/packshoes.rb in your Shoes. Below is an example and of course it's very specific to my demo app and build system.

```ruby
require 'shoes/packshoes'
opts = {}
opts['app'] = '/home/ccoupe/Projects/ytmtest/ytm-repack.shy'
opts['advopts'] =  true
opts['expandshy'] = true
#opts['custominstaller'] = '/full/path/to/my/install_script.rb'
opts['installer-icon'] = '/home/ccoupe/Projects/shoes3/static/shoes-icon-federal
es.png'
opts['png'] = '/home/ccoupe/Projects/icons/ytm/ytm.png'
opts['ico'] = '/home/ccoupe/Projects/icons/ytm/ytm.ico'
opts['icns'] = '/home/ccoupe/Projects/icons/ytm/ytm.icns'
#opts['gempack'] = '/home/ccoupe/Projects/gems/gempacks/kevin.tar.gz'
opts['arch'] = 'x86_64' # win32 armhf x86_64 i686 or osx
opts['dnlhost'] = 'walkabout.mvmanila.com'  # no http://
opts['dnlpath'] = "/public/select/#{opts['arch']}.rb"
opts['shoesdist'] = "/home/ccoupe/.shoes/walkabout/package/shoes-3.3.1-gtk3-x86_
64.install"
opts['packtmp'] = '/home/ccoupe/.shoes'
opts['relname'] = 'walkabout'
PackShoes.repack_linux opts
```
Since we're using a hash, the order doesn't really matter but I'm going to start the discussion with minimal settings. Many of those above are not minimal 
#### opts['app']

Is the full pathname to the script (or .shy) to package. Frankly you're going to want to use a shy to do anything useful for the end user. I've shown a script to could use to package a .shy above or you could do it from the Shoes Gui.  Remember. you can package a shy as a Download If Needed (dnlif) or "Include Shoes" (repack).

#### opts['dnlhost'] and opts['dnlpath']

Is where to download the Shoes distribution from. This is passed into the _stub_ that runs on the users system
dnlpath is the cgi script to run at dnlhost. That will return a full shoes.exe (or osx tgz or linux.install) that will be installed if needed.

#### opts['shoesdist']

Is a the full path to the where Shoes previously downloaded the Shoes.exe you want to repack. In Windows, it will be in `HOME\AppData\` probably `AppData\shoes\walkabout\package` (`~/.shoes/walkabout` for OSX and Linux) but you'll want to verify that for yourself. It's important to have your choice downloaded first if you do a repack. Easy to do with the packager GUI - just pack (anything) for the architecture you want and if you don't have in walkabout/package the GUI will download the latest. 

#### opts['relname']

I'm not sure if this used or not. Won't hurt to include it.

#### opts['winargs']

This is optional and only applies to Shoes 3.2.23 or newer and only for Windows. `opts['winargs']='--console'` causes the packaged app to open a command window along with the Shoes app windows. Handy if your app uses Ruby puts and/or C printfs and you want the user to see them.  The console window is closed when the Shoes app is terminated. 

This will be deprecated soon because if you need this, you can call `Shoes::shoe_console` in your script and that is cross platform and not Windows specific.

#### which method in PackShoes?

dnlif_exe, repack_exe, dnlif_osx, repack_osx, dnlif_linux, repack_linux.

You can add a block to the repack_??? calls but other than printing the progress messages it not very useful. 

#### arch

You do need to set this for whatever your packaging for. Particularly if it's dnlif. Values are 'win32' 'armhf' 'x86_64' 'i686' or 'osx'

#### packtmp

At this point in time, there is no default. Its a  place or your computere to dissemble the downloaded, create shy things, and reassemble them. After packaging, this may or may not be deleted. 

### What about those other opts[values]? 

Those are for folks that want more control. opts['advopts'] is your gateway. If it's set false or not given you still have some icons setting ability. See [[Custom-Install-Scripts]]

#### Icons 
The default for icons in the download and install by your users are the silly Shoes icons. There is no way to change them in the GUI but if you want your icons in the exe or attached to your OSX.app you can do do by specifying
```ruby
opts['ico'] = "/full/path/to/myapp.ico'  # Windows
opts['icns'] = "/full/path/to/myapp.icns" # OSX
opts['png'] = "/fuul/path/to/myapp.png"  # Linux
```
If you have gone to the trouble of creating you own icon, you might as well create and include all three and include them in your packaging script. See [[Creating-Icons]]

#### opts['advopts'] = true
Beware of magic ahead. This choice will package your .shy inside another _installer_ shy. Your shy is the _inner_ shy. When your app is installed on the users system, Shoes will will run a script in the outer outer shy.  We supply a default script, but you can supply your own. That script should be a Shoe script. The following options will only work if you have `opts['advopts'] = 'true'`

#### opts['gempack']

One thing you can include in that outer shy is a **gem pack** which is a pre-built set of gems for the architecture of your choice. See [[Gempacks]]

#### opts['expandshy']

When set to 'true' your 'inner shy' will be expanded at install time and it will be copied to a user given location. (default script behavior)

#### opts['installer-icon']

Some times your app icon needs to be different from your installer icon. This png is displayed by the default 2nd level installer. 

#### opts['custominstaller'] = '/path/to/myapp-installer.rb'

If you've looked at the default custom installer (static/stubs/app-install.tmpl), you're probably said "what a dogs mess for breakfast!, I don't need that cross platform crap for my app! It's thirteen ways to Sunday ugly!" Then you can provide your own script here. We don't mind. We won't even know. 

