# Profiling

Shoes does have a simple profiler that can help you locate often used  code and time consuming code. This is **only** available when run from the  command line. See [Command line](configuration.md#command-line) for help.

So from the terminal `C:\Users\you> cshoes.exe -e path\to\main-script.rb` On OSX and Linux it could be `$ cshoes -e path/to/main-script.rb` assuming  you've created a cshoes script (osx) or link (linux)

Pretend we want to see where samples/good-cardflip.rb is spending it's time. `$ cshoes -e samples/good-cardflip.rb` your path will depend on what you need.


You'll get Shoes app screen that looks a lot like
![profile-start](https://cloud.githubusercontent.com/assets/222691/17391141/fac141e0-59d0-11e6-8ba3-d3858879c7fe.png)

I want to know what C parts of Shoes I'm calling and I prefer the terminal display so that what I unselected. You can switch between gui and textlater after the profile is run.  When you press the `Start Profile` button it will
load and start the script and collect stats until you press the `End Profile` button. Then you'll get a display. Because of the way Shoes opens new windows The sampled app window tends to be centered on the screen and covering the
Profiler screen. Move the Profiler to the side before starting or move sampled window until you can see the End Profile button.

Here's the beginning shot for the text report:
![profile-text](https://cloud.githubusercontent.com/assets/222691/17391634/ef25890a-59d4-11e6-8a25-057fa38ff0cd.png)

The gui display:
![profile-gui](https://cloud.githubusercontent.com/assets/222691/17391606/b514297e-59d4-11e6-8d88-fecf0469f008.png)

What does it mean? 

You have to be **very** careful interpeting the results. On Windows, we can't  get the cpu time per method call. Just the wall clock time which includes how long it takes you to manipulate your buttons and navigate around. OSX and Linux do get the cpu time for the process but there is still some slop in there.
Take the clock time problem up with MSFT. And the Ruby. May the force be with you! 

Count is the number of calls and draw is always going to be near the vote count.
I suggest `Method Time` or mtime is where you want to look. `Total Time` or t-time is a little confusing since it includes all the time in methods below it. 
The text report does compute ms per call which can point to the real problem. 
Remember - on Windows that's wall clock time not cpu time. 

In this example svghandle is the pig. I knew that before profiling. If your Shoes app uses sqlite3 you'll want to think really hard about what is being measured and not measured on what platforms.

