## Getting started

### Download
Shoes is available for Windows (from xp to 10), OSX and linux and BSD. You can find the latest version here : 

- [Windows](https://walkabout.mvmanila.com/downloads/windows-downloads/)
- [OSX](https://walkabout.mvmanila.com/downloads/osx-downloads/)
- [Linux and BSD](http://walkabout.mvmanila.com/downloads/linux-downloads/)

There is no upgrade system for now, but you can manually upgrade, by uninstalling one version and installing another one.

### Installation

Okay, on to installing Shoes. I'm sure you're wondering: do I need to install
Ruby? Do I need to unzip anything? What commands do I need to type?

Nope. You don't need Ruby. You don't need WinZip. Nothing to type.

On most systems, starting Shoes is just a matter of running the installer and
clicking the Shoes icon. Shoes comes with everything built in. We'll talk
through all the steps, though, just to be clear about it.

#### Step 1: Installing Shoes

Here's how to run the installer:

**Mac OS X**

Using [Homebrew](https://brew.sh/) :
simply run `brew cask install shoes`

Other way :
 - You'll have a file ending with *.tbz* in the Downloads folder. 
 - Double-click this file and a file should appear with a *Shoes* icon. 
 - You may be asked to trust the download package. 
 - Drag the Shoes icon into the *Applications* folder. 

**Windows**

 - You'll download a *.exe* file. 
 - Double-click this file and follow the instructions.

**Linux**

  - Make the *.install* file executable with `chmod +x Shoes-3.2-stuff.install`
  - Run it with the following command : `./Shoes-3.2-stuff.install`
  - It will ask for the admin password to create a menu entry.

#### Step 2: Start a New Text File

Shoes programs are just plain text files ending with a *.rb* extension.

Here are a few ways to create a blank text file:

**Mac OS X**

Visit your *Applications* folder and double-click on the *TextEdit* app. On the new windows click on `New Document`. A blank editor window should come up. Now, go to the *Format* menu and select the *Make Plain Text* option. Okay, you're all set!

![new blank file in osx](img/getting_started_osx-new-file.png)

**Windows**

Go to the Start menu. Select *All Programs*, then *Accessories*, then *Notepad*. 

TODO : image

**Linux**

Most distros come with *gedit*. You might try running that. Or, if your distro is KDE-based, run *kate*.

![new blank file in linux](img/getting_started_linux-new-file.png)

**Your first Shoes app**

Now, in your blank window, type in the following:

```ruby
 Shoes.app do
   background "#DFA"
   para "Welcome to Shoes"
 end
```

Save to your desktop or home directory as `welcome.rb`.

#### Step 3: Run It! Go Shoes!

There are several ways to run a Shoes program. The easiest is from the 'splash' screen.
We're going to use the *Open an App* link to browse to where you stored
`welcome.rb`.

![simple shoes app](img/getting_started_simple-app.png)

To run your program, start Shoes:

**Mac OS X**

Visit your *Applications* folder again. This time, double-click the *Shoes* icon in that folder. You should see the splash screen above. 

**Windows**

Get to the Start menu. Go into *All Programs*, then *Shoes*, then *Shoes*. You should see the splash screen above.

**Linux**

Look for the Shoes icon in the menus Programming, Education, or Others. Select it. You should see the splash screen above.

So, not much of a program yet. But it's something! You've got the knack of it, at least! 

#### What Can You Make With Shoes?

Well, you can make windowing applications. But Shoes is inspired by the web, so
applications tend to use images and text layout rather than a lot of widgets.
For example, Shoes doesn't come with tabbed controls or toolbars. Shoes is a
''tiny'' toolkit, remember?

Still, Shoes does have a few widgets like buttons and edit boxes. And many
missing elements (like tabbed controls or toolbars) can be simulated with
images.

Shoes is written in part thanks to a very good art engine called Cairo, which
is used for drawing with shapes and colors. In this way, Shoes is inspired by
NodeBox and Processing, two very good languages for drawing animated graphics.
