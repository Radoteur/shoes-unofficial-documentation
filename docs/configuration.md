# Configuration
Shoes has lots of features that might do what you want if only someone would write it down so you can find it and then you can experiment and learn more. Some magic may require you to use the command line but you'll become a more knowledgable Shoes developer when you decide to use the command line.

At a minimum, you'll learn what else Shoes can do, so skim these articles and then you can read them when your ready for extra fun. Just remember your goal.

Try something **you can't hurt Shoes** it can always be re-installed. Not everything has to be absorbed at once to have fun learning.

## Files location
There are many files in a Shoes installation. You might want to know where they are if you are troubleshooting a problem. For this discussion I'm going to talk about Shoes (the program), Gems, Caches, and temporary directories. One principle of Shoes is to not use system directories.

If possible,  Shoes always uses User directories. Please read the Linux description even if you use OSX or Windows.

### Linux:

- Shoes code and libraries including the manual and icons, is installed in `~/.shoes/federales`.
- Gems are installed in `~/.shoes/+gem`. Yes the `+` is part of the filename. No, I don't know why.
- Image cache files are stored in `~/.shoes/+cache`.
- If you've been packaging with `include shoes`, there is a cache of those downloads in `~/.shoes/federales/package`.
- Desktop menus and icons follow the xdg specifications so they are place where ever xdg on your box thinks they should be.
- Temporary directories are in /tmp unless they are in `~./shoes`

### OSX:

OSX is a lot like Linux. Most things are kept in `~/.shoes` except the Shoes code lives in `Shoes.app/Contents/MacOS`. You can drag the Shoes.app Icon (program) anywhere you like.

Temporary directories live in some `/private/var` hell that only Apple and BSD/Mach could create.

### Windows:

If you accepted the defaults on the installer (you don't have to), then  Shoes is installed in `C\:Program Files (x86)\Shoes\shoes-3.2.xy\` (yes you might have older versions stuck in there, and yes you do not want that).

Everything else is in AppData which Windows in its benevolence **does not show** you.  You Google "Show AppData Folder" or something like that and follow the instructions. After you do that, you'll find `AppData\Local\Shoes` which has the gems and federales and image cache. Temp directories are in `Appdata\Local\Temp`

### Inside The Shoes folder
  Feel free to browse through it. `lib/shoes/*.rb` might be of interest. Beware, there is dead code in there that Shoes 3.2 doesn't use. Changing a file in there? Please don't. If you really, really want to do that then you should promote yourself to Shoes Maintainer and there's a butt load of docs to read and learn.


## Command line
Terminal jockeys rejoice. Shoes has always had a command line but no one talks about it. Shoes 3.2 even helps Shoes/Windows behave better when run from the console.

Shoes may spit out a warning message or two when run from the console.
Many of those warnings are not a problem with Shoes :

- Windows complains about fontconfig (but fontconfig isn't use so there you go).
- The lastest Raspbian spits out all manner of Gtk warnings. It's not Shoes - it does that for lots of other GUI programs on the Pi.

### Switches

- `-h`   displays the help
- `-m`   displays the manual
- `-c`   displays the Cobbler (Maintain Shoes)
- `-d`   invokes the byebug debugger
- `-e`   profile the script
- `-g`   sends the rest of the command line to Ruby gems
- `--ruby` send the rest of the command line to Ruby (no Shoes gui)

The last two deserve some explanations.  Shoes has its own copy of ruby and gem programs. You can use them to do anything `$ gem <strings>` and `$ ruby <strings>` would do.

Examples:

- `-g` env   is equivalent to gem env - tells about the Gem setup
- `-g install xxx`   would install gem xxx
- `--ruby -e "puts RUBY_PLATFORM"`
- `--ruby my-non-gui-script.rb`

Profiling and debugging have their own Wiki pages.

You wouldn't want to launch a Webrick or Jekyll server that way but you could try and it might work.

Shoes will with call itself with a `--ruby` command when installing some Gems from a Shoes Gem.setup. Don't push the `--ruby` too hard, that Ruby inside is not a substitute for installing Ruby if you want a general purpose Ruby command line.

There are SHOES unique constants and ENV settings defined in the Ruby started by `--ruby` and `-g`.  It's handy for some situations.

### Linux

 As mentioned in "where's my files", the Linux Shoes binary is  `~/.shoes/federales/shoes`
From the terminal `~/.shoes/federales/shoes -h` (or `-m`) or `\<path-to-script.rb\>` or path to `.shy`

If you'd like to shorten that then One _could_ do `sudo ln -s ~/.shoes/walkabout/shoes /usr/local/bin/cshoes` or
set an alias `$ alias cshoes='~/.shoes/walkabout/shoes'` in your `~/.bashrc` Both
have tradeoff's that Shoes won't make for you. If it was me, and it's not, and I have
`~/bin` in my `$PATH` I'd lean towards `ln -s ~/.shoes/walkabout/shoes ~/bin/cshoes`
But that's just me.


### OSX

To use Shoes from the command line you'll need to create a `cshoes` script.

It's easy to do :

1. Quit Shoes if running
2. Launch Shoes again
3. Select "Maintain Shoes"
4. Click on the `Setup` menu then `Setup ./cshoes`
5. Select the place where you want to save your script
6. Click on `Create Script`
7. You can run the script from the place you picked

**What could go wrong?**

Since you can create as many cshoes as you like and place them all over you directories and they are hard-coded to where you installed Shoes, if you decide to move Shoes later, then all those old cshoes scripts will not work. Of course, if you put cshoes in `/usr/local/bin` and it pointed to `/Applications/Shoes.app` then you'd only have one copy that worked in all your projects. As a command line commando you knew that.

### Windows:

  Shoes 3.2 has two small exe's, `shoes.exe` and `cshoes.exe`, the difference is `cshoes.exe` is for running shoes from a Console. Any puts or C level printf output is displayed. Use `cshoes.exe` if you want to use the console which is what this section is about.
  
## Ruby Gems
Cobbler is an internal tool for Shoes that helps with many internal operations including managing gems, [here's article about Cobbler from it's creator](http://walkabout.mvmanila.com/2014/05/26/this-is-why-shoes-3-2-exists/]).

Shoes has it's own gem directory (`$HOME/.shoes/+gems`) containing pre-installed gems and there is a way tell Shoes to check for and install new gems to there.

**Not all gem can be installed in Shoes.**

There are three kinds of gems that you might encounter :

- **Pure Ruby** : Gems that only use Ruby are easily installed by Shoes
- **Source code** : What some of us call source are really gems that have C source code that has to be compiled when installed - That's a pain for Windows and OSX folks who don't have the development tool
- **Binary** :  Recently, there are gems available that actually have the C source precompiled (usually for Windows but only the MingW ones can possibly work with Shoes). So, if you arer running Windows and your Gem installs with Shoes.setup,  take a few moments to acknowledge how lucky you are.

In Federales (shoes 3.2) there is a way (or two or three and a half) to get out of the Shoes Sandbox. Tight Shoes is a downloaded Shoes. Loose Shoes is one you built from source. Different rules. For Tight Shoes, You can add a directory (or more) to the list of directories that Shoes's Ruby will search through to find gems. If you already have a Ruby and gems installed on your system, then you just tell Shoes where those gems are. I call it a Jailbreak. There's a Cobbler option to do that for you. Just like breaking out a real jail, you can get in deep do-do and it's up to you to see the problems and fix it. No big deal if you know what your're doing.  You'll avoid a lot of trouble if you install the exact Ruby that Shoes is using. If it's 2.1.0 in Shoes then that what you want to use to install gems. If Shoes is 2.1.2, install that Ruby and then you can use that ruby `gem install` from the commandline.  It's all up to you to manage the versions and directories.  Consider yourself empowered. Avoid evil.

### Loose Shoes behavior

Loose Shoes (that you built from Shoes source) already uses your installed gems and ruby. It's intimately tied to them. There is not a copy of Ruby inside a Loose Shoes it uses what ever Ruby it was built with. You can package a script with Loose Shoes for Linux, OSX and Windows. What you can't do with Loose Shoes is make an Shoes installer. (rake package). If you're confused about that restriction, think about how a linking loader works - repeat until the light bulb goes off.
    
### Pre-build binaries

 It is *possible* to distribute pre-built binary gems (all platforms) if and **only If** you can build them (somewhere). This violates almost everything done in the last few years to complexify ruby gems.

Nokogiri is a fine example (it's now included with Shoes so our process can work).  If you can navigate the nokogiri build process, you can create a nokogiri precompiled binary gem - For Windows and just Windows and it's a huge gem, larded up with things Shoes doesn't need or could use. Precompiled for OSX ? No. Linux binaries? No. ARM ? No. MIPS? All possible but why bother if you believe everybody has Ruby and Dev tools installed?  A perfectly reasonable assumption for Ruby Gem writers. Nokogiri also requires building the Windows code on Linux or OSX - won't do it on Windows. Their rake compiler process is conflicts with how Shoes is built - not that Shoes couldn't be fixed but it's a lot to swallow and get nothing Shoes useful for the effort.

## Cache

Images and libraries are caches inside Shoes, if you wish to clean the cache for actualizing the content of your library you can do so by opening Cobbler (`Maintain Shoes`link when starting Shoes or `Cobbler` in the menu). 

When the Cobbler windows is visible select the `Setup` menu then `Clear Cache` this restart Shoes and reload your libraries and images when opening your project.
