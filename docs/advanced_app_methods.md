# AppMethods 

There are some special methods associated with App that are designed for special situations. They affect subtle things and big things. They are a bit dangerous if called incorrectly so experiment wisely if you need the feature. You would want to call these early in your Shoes.app block. 

Another caution - these are methods of an App instance so you need that object. Even when they effect more than one instance of App. Shoes creates an 'app' method in the Shoes.app block that you must use for the following.

## app.cache » boolean 

Returns the setting of the app's image caching status.

## app.cache = boolean 

Changes the image caching behavior for the whole app. By default, images are  cached (aka 'true'). Sometimes, that is not desired. You can turn off the caching for all image calls by setting this to 'false' Note: individual calls to image can override the global setting - see [images](elements.md#image).

## app.cache_clear sym » boolean

Clears the internal image cache or the external image cache or both depending on the  symbol provided for the argument - one of :memory, :external, :all

Always returns true. 

## app.cursor » symbol 

Returns the cursor symbol currently set for the Window (app). 

## app.cursor = symbol 

Sets the cursor to the symbol. The symbols is one of the following: 
`:arrow_cursor`, `:text_cursor`, `:watch_cursor` or `:hand_cursor`.

```ruby
Shoes.app do
  stack do
    para "Change cursor to"
    flow do
      button "arrow" do
        app.cursor = :arrow_cursor
      end
      button "text" do
        app.cursor = :text_cursor
      end
      button "watch" do
        app.cursor = :watch_cursor
      end
      button "hand" do
        app.cursor = :hand_cursor
      end
    end
    para "Cursor is #{app.cursor.inspect}"
  end
end
```

## app.decorated » true or false 

Decorations are the title bar and window resize controls. 

Returns true or false whether the window has decorations or not. See below for the issues involved.

## app.decorated = true or false 

Warning: This is highly platform/theme specific.  DO NOT assume that if it works for you, that it will work for anyone else. It won't work for everyone. OSX for example. Combined with fullscreem - you are asking for trouble. Think twice or thrice.

Set window decorations. `true` will display titlebar, and resize buttons and `false` will hide them.

## app.fullscreen » boolean 

This returns true or false if your script is running in full screen mode

## app.fullscreen = Boolea) 

Set fullscreen to true or false. If you do this, you should provide a method for turning it off. There's nothing more annoying that than a full screen app that the user can't figure out how to quit and get back to normal. 

:fullscreen => true is also a style you can set for Shoes.app.

## app.id » string 

Returns a unique string for the app (window). This is different from name or title which are not unique. Used internally. 

## app.monitor » integer 

Returns the small integer for the monitor (screen) the app is on. For systems with only one monitor this will be 0. See the Advanced section of this manual for Settings and Monitor.

## app.monitor = integer 

Moves the app (window) to the <integer> monitor. See the Advanced section of this manual for Settings and Monitor.

## app.name » string 
Returns the title string for the App. Usually the string in the window's title bar.

## app.name = String 

Sets the title string of the current window. See Advanced/Settings or app.set_window_title below to make the title string apply to new windows.

## app.location » shoes-url 

Returns the Shoes url. Be careful. It's a shoes-url so it might not be what
you think.

## app.resize width, height » boolean 

Change the size of the window. You don't want to increase beyond your or your users screen size - That could be a problem (crash?). Also it can misbehave if  you shrink it smaller than the original creation size. Always returns True  but there is no error checking. Use cautiously.

## app.started? » boolean 

Is the app completely initiailized? 

## app.width » a number 

Returns the current width.

## app.height » a number 

Returns the current height. Perhaps you need it and the width after going fullscreen to setup your game to the size availavble. 

## app.opacity » a float number 

Returns a float number of the current window opacity. The default opacity is `1.0`  where you see the window without transparency. An opacity of `0.0` is fully transparent window.

## app.opacity = a float number 

Set the window opacity with a float number. A fully transparent window has an opacity of `0.0`  whereas a fully visible window has an opacity of `1.0`.

```ruby
Shoes.app do
   @b = banner "Opacity\n"

   slider fraction: 1.0 do |n|
      app.opacity = n.fraction
      @b.text = "Opacity %.2f\n" % n.fraction
   end
end
```

## app.slot » Array 

For Advanced users: 

When you want to manage slots and elements from another Shoes.app window you need to gets it's app and then get it's slots.contents and then you can poke around in in the  stacks and flows and elements. 

This is the preferred method for getting an array of stacks and flows for the windows that Shoes.APPS[] knows about.  

For example you have two windows (apps) and want to append a para to all but a specific window (app)

```ruby
 Shoes.app title: "Controller" do
   $other_win =  window title: "Slave" do
     stack { para "First"} 
   end
   stack do
     button "Append" do
       Shoes.APPS.each do |ap| 
         ap.slot.contents[0].instance_exec () {para "gotcha"} if ap == $other_win
       end
     end
   end
 end
```

There is much more to discover for advanced users on this topic. See IRB in this manual and at the wiki.

## app.set_window_icon_path(pathname) 

This sets the icon in the title bar (and the dock or taskbar of your OS) for this window and any others you create from this script. It replaces the little Shoes federales icon. If you need this, do it for every app.  Depending on your OS, theme and desktop settings, the icon may not be displayed at all.

See Advanced->Settings for additional possibilies.

```ruby
 Shoes.app :title => "I'm Shoes" do
    button "feeling blue" do
      Shoes.APPS.each {|a| a.set_window_icon_path("#{DIR}/static/shoes-icon-blue.png") }
      confirm "did all the Shoes icons change? "
    end
 end
```
The icon should be a 128x128 png. Or 256x256 png. 512x512 also works.

## app.set_window_title(string) 

This sets the string in the current title bar like name = does but also the default title every new window you might create including the alert, ask and confirm dialogs.  What it does not do is change the title of other existing windows.

Note: the :title style setting only effects one window. This effects all ```NEW``` windows. 

If you need this, loop through all the Apps.

```ruby
Shoes.app :title => "I'm Shoes" do
   button "change title" do
     Shoes.APPS.each {|a| a.set_window_title("No Shoes Here") }
     confirm "did the title change to No Shoes Here"
   end
end
```

Notice how the title is very sticky. 
