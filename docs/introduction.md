# Introduction
Shoes is all about drawing windows and the stuff inside those windows.  Let's focus on the window itself, for now.  The other sections [Slots](slots.md) and [Elements](elements.md) cover everything that goes inside the window.

For here on, the manual reads more like a dictionary. Each page is mostly a list of methods you can use for each topic covered. The idea is to be very thorough and clear about everything.

## The Rules of Shoes 

Time to stop guessing how Shoes works. Some of the tricky things will come back to haunt you. I've boiled down the central rules to Shoes. These are the things you MUST know to really make it all work.

These are general rules found throughout Shoes. While Shoes has an overall philosophy of simplicity and clarity, there are a few points that need to be studied and remembered.

### Shoes Tricky Blocks

Okay, this is absolutely crucial. Shoes does a trick with blocks. This trick makes everything easier to read. But it also can make blocks harder to use once you're in deep.

*Let's take a normal Ruby block:*

```ruby
 ary = ['potion', 'swords', 'shields']
 ary.each do |item|
   puts item
 end
```

In Shoes, these sorts of blocks work the same. This block above loops through the array and stores each object in the `item` variable. The `item` variable disappears (goes out of scope) when the block ends.

One other thing to keep in mind is that `self` stays the same inside normal Ruby blocks. Whatever `self` was before the call to `each`, it is the same inside the `each` block.

*Both of these things are also true for most Shoes blocks.*

```ruby
 Shoes.app do
   stack do
     para "First"
     para "Second"
     para "Third"
   end
 end
```

Here we have two blocks.  The first block is sent to `Shoes.app`.  This `app` block changes `self`.

The other block is the `stack` block.  That block does NOT change self.

*For what reason does the `app` block change self?*  Let's start by spelling out that last example completely.

```ruby
 Shoes.app do
   self.stack do
     self.para "First"
     self.para "Second"
     self.para "Third"
   end
 end
```

All of the `self`s in the above example are the App object.  Shoes uses Ruby's `instance_eval` to change self inside the `app` block.  So the method calls to `stack` and `para` get sent to the app.

*This also is why you can use instance variables throughout a Shoes app:*

```ruby
 Shoes.app do
   @s = stack do
     @p1 = para "First"
     @p2 = para "Second"
     @p3 = para "Third"
   end
 end
```

These instance variables will all end up inside the App object.

*Whenever you create a new window, `self` is also changed.*  So, this means the [window](slots.md#windowstyles-shoesapp) and [dialog](slots.md#dialogstyles-shoesapp) methods, in addition to Shoes.app.

```ruby
 Shoes.app :title => "MAIN" do
   para self
   button "Spawn" do
     window :title => "CHILD" do
       para self
     end
   end
 end
```

### Block Redirection

The `stack` block is a different story, though.  It doesn't change `self` and it's basically a regular block.

*But there's a trick:* when you attach a `stack` and give it a block, the App object places that stack in its memory.  The stack gets popped off when the block ends.  So all drawing inside the block gets *redirected* from the
App's top slot to the new stack.

So those three `para`s will get drawn on the `stack`, even though they actually get sent to the App object first.

```ruby
 Shoes.app do
   stack do
     para "First"
     para "Second"
     para "Third"
   end
 end
```

A bit tricky, you see?  This can bite you even if you know about it.

One way it'll get you is if you try to edit a stack somewhere else in your program, outside the `app` block.

Like let's say you pass around a stack object.  And you have a class that edits that object.

```ruby
 class Messenger
   def initialize(stack)
     @stack = stack
   end
   def add(msg)
     @stack.append do
       para msg
     end
   end
 end
```

So, let's assume you pass the stack object into your Messenger class when the app starts.  And, later, when a message comes in, the `add` method gets used to append a paragraph to that stack.  Should work, right?

Nope, it won't work.  The `para` method won't be found.  The App object isn't around any more.  And it's the one with the `para` method.

Fortunately, each Shoes object has an `app` method that will let you reopen the App object so you can do somefurther editing.

```ruby
 class Messenger
   def initialize(stack)
     @stack = stack
   end
   def add(msg)
     @stack.app do
       @stack.append do
         para msg
       end
     end
   end
 end
```

As you can imagine, the `app` object changes `self` to the App object.

So the rules here are:

1. *Methods named "app" or which create new windows alter `self` to the App object.* (This is true for both Shoes.app and Slot.app, as well as [window](slots.md#windowstyles-shoesapp) and [dialog](slots.md#dialogstyles-shoesapp).)
2. *Blocks attached to stacks, flows or any manipulation method (such as append) do not change self. Instead, they pop the slot on to the app's editing stack.*

### Careful With Fixed Heights

Fixed widths on slots are great so you can split the window into columns.

```ruby
 Shoes.app do
   flow do
     stack :width => 200 do
       caption "Column one"
       para "is 200 pixels wide"
     end
     stack :width => -200 do
       caption "Column two"
       para "is 100% minus 200 pixels wide"
     end
   end
 end
```

Fixed heights on slots should be less common.  Usually you want your text and images to just flow down the window as far as they can.  Height usually happens naturally.

The important thing here is that fixed heights actually force slots to behave differently.  To be sure that the end of the slot is chopped off perfectly, the slot becomes a *nested window*.  A new layer is created by the operating system to keep the slot in a fixed square.

One difference between normal slots and nested window slots is that the latter can have scrollbars.

```ruby
 Shoes.app do
   stack :width => 200, :height => 200, :scroll => true do
     background "#DFA"
     100.times do |i|
       para "Paragraph No. #{i}"
     end
   end
 end
```

These nested windows require more memory.  They tax the application a bit more. So if you're experiencing some slowness with hundreds of fixed-height slots, try a different approach.

### Image and Shape Blocks

Most beginners start littering the window with shapes.  It's just easier to throw all your rectangles and ovals in a slot.

*However, bear in mind that Shoes will create objects for all those shapes!*

```ruby
 Shoes.app do
   fill black(0.1)
   100.times do |i|
     oval i, i, i * 2
   end
 end
```

In this example, one-hundred Oval objects are created.  This isn't too bad. But things would be slimmer if we made these into a single shape.

```ruby
 Shoes.app do
   fill black(0.1)
   shape do
     100.times do |i|
       oval i, i, i * 2
     end
   end
 end
```

Oh, wait.  The ovals aren't filled in this time!  That's because the ovals have been combined into a single huge shape.  And Shoes isn't sure where to fill in this case.

So you usually only want to combine into a single shape when you're dealing strictly with outlines.

Another option is to combine all those ovals into a single image.

```ruby
 Shoes.app do
   fill black(0.1)
   image 300, 300 do
     100.times do |i|
       oval i, i, i * 2
     end
   end
 end
```

There we go!  The ovals are all combined into a single 300 x 300 pixel image. In this case, storing that image in memory might be much bigger than having one-hundred ovals around.  But when you're dealing with thousands of shapes, the image block can be cheaper.

The point is: it's easy to group shapes together into image or shape blocks, so give it a try if you're looking to gain some speed.  Shape blocks particularly will save you some memory and speed.

### UTF-8 Everywhere

Ruby is now very Unicode aware.  And UTF-8 is a type of Unicode.  (See [Wikipedia](http://en.wikipedia.org/wiki/UTF-8) for a full explanation of UTF-8.)

However, UTF-8 is common on the web.  And lots of different platforms support it.  So to cut down on the amount of conversion that Shoes has to do, Shoes expects all strings to be in UTF-8 format.

This is great because you can show a myriad of languages (Russian, Japanese, Spanish, English) using UTF-8 in Shoes.  Just be sure that your text editor uses UTF-8!

To illustrate:

```ruby
 Shoes.app do
   stack :margin => 10 do
     @edit = edit_box :width => 1.0 do
       @para.text = @edit.text
     end
     @para = para ""
   end
 end
```

This app will copy anything you paste into the edit box and display it in a Shoes paragraph.  You can try copying some foreign text (such as Greek or Japanese) into this box to see how it displays.

This is a good test because it proves that the edit box gives back UTF-8 characters.  And the paragraph can be set to any UTF-8 characters.

*Important note:* if some UTF-8 characters don't display for you, you will need to change the paragraph's font.  This is especially common on OS X.

So, a good Japanese font on OS X is *AppleGothic* and on Windows is *MS UI Gothic*.

```ruby
 Shoes.app do   para "てすと (te-su-to)", :font => case RUBY_PLATFORM
    when /mingw/; "MS UI Gothic"
    when /darwin/; "AppleGothic, Arial"
    else "Arial"
    end
 end
```

Again, anything which takes a string in Shoes will need a UTF-8 string. Edit boxes, edit lines, list boxes, window titles and text blocks all take UTF-8. If you give a string with bad characters in it, an error will show up in the console.

### App and Its Scope

Modern Shoes uses TOPLEVEL_BINDING.  Binding is not an easy concept so anything you read here needs a large grain of salt. Any method or class defined before Ruby  reads the Shoe.app {block} belongs to Ruby 'main' binding and anyhing defined inside the Shoes.app {block} belongs to that app binding. Inside the app binding you can call on those `main` bound methods but those methods can't (easily) invoke methods in a Shoes.app binding. Confused? Join the crowd.

Each Shoes.app {} or Window {} you create in your scripts gets a different `binding` so the variable "my_button" in one window is a different object from a variable "my_button" in  the other window (for example). Different windows, different bindings for the same name.

This isolates one window from another. Some consider this a `good thing`. It's not going to change however,  so some say `live with it` [Poking in Shoes](https://github.com/Shoes3/shoes3/wiki/Poking-in-Shoes.app) explore how to use APP[] and app but it only a starter course in a very long menu. 

For complex, multi-window Shoes apps that need to share some things and not others it's still unknown best practices. There might be more information in [Structuring](structuring.md).

## Built-in Methods 

**These methods can be used anywhere throughout Shoes programs.**

All of these commands are unusual because you don't attach them with a dot. *'Every other method in this manual must be attached to an object with a dot.*' But these are built-in methods (also called: Kernel methods.) Which means no dot!

A common one is `alert`:

```ruby
 alert "No dots in sight"
```

Compare that to the method `reverse`, which isn't a Kernel method and is only available for Arrays and Strings:

```ruby
 "Plaster of Paris".reverse
  #=> "siraP fo retsalP"
 [:dogs, :cows, :snakes].reverse
  #=> [:snakes, :cows, :dogs]
```

Most Shoes methods for drawing and making buttons and so on are attached to slots.  See the section on [Slots](slots.md) for more.

#### Built-in Constants

Shoes also has a handful of built-in constants which may prove useful if you are trying to sniff out what release of Shoes is running.

Shoes 3.2.22 introduced new constants that you should use instead of the older ones.

- `Shoes::VERSION_NUMBER` is a string like `3.2.22`
- `Shoes::VERSION_MAJOR` is a number, the first one of VERSION_NUMBER
- `Shoes::VERSION_MINOR` is a number, the second one in VERSION_NUMBER
- `Shoes::VERSION_TINY` is a number, the last in VERSION_NUMBER
- `Shoes::VERSION_NAME` is a string with the name of the Shoes release, like `federales`
- `Shoes::VERSION_REVISION` is a number. Probably the count of commits but it could have been set to some other number by the builder.
- `Shoes::VERSION_DATE` is a string. It's the build date.
- `Shoes::VERSION_PLATFORM` is a string. It's probably the platform your Shoes was built on which **is not the same** as `RUBY_PLATFORM`
- `Shoes::FONTS` is a complete list of fonts available to the app.  This list includes any fonts loaded by the [font](introduction.md#fontmessage-a-string-an-array-of-font-family-names) method.

Deprecated constants:

- `Shoes::RELEASE_NAME` contains a string with the name of the Shoes release. All Shoes releases are named, starting with Curious.
- `Shoes::RELEASE_ID` contains a number representing the Shoes release. So, for example, Curious is number 1, as it was the first official release.
- `Shoes::REVISION` is the Subversion revision number for this build.

### alert(message: a string, :title => a string or nil) » nil 

Pops up a window containing a short message.

```ruby
 alert("I'm afraid I must interject!")
```

`:title` is optional and replaces the default "Shoes says:" with the string you provide or could be set to `nil` in which case the window won't have a title at all.

```ruby
 alert("I'm afraid I must interject!", :title => "Sorry but :")
```

```ruby
 alert("I'm afraid I must interject!", :title => nil)
```

Please use alerts sparingly, as they are incredibly annoying!  If you are using alerts to show messages to help you debug your program, try checking out the [debug](introduction.md#debugmessage-a-string-nil) or [info](introduction.md#infomessage-a-string-nil) methods.

### ask(message: a string, :title => a string or nil) » a string 

Pops up a window and asks a question. For example, you may want to ask someone their name.

```ruby
 name = ask("Please, enter your name:")
```

When the above script is run, the person at the computer will see a window with a blank box for entering their name. The name will then be saved in the `name` variable.

`:title` is optional and replace the default "Shoes asks:" with the string you provide or could be set to `nil` in which case the window won't have a title at all. 

See [alert](introduction.md#alertmessage-a-string-title-a-string-or-nil-nil) method for similar examples.

### ask_color(title: a string) » Shoes::Color 

Pops up a color picker window. The program will wait for a color to be picked, then gives you back a Color object. See the `Color` help for some ways you can use this color.

```ruby
 backcolor = ask_color("Pick a background")
 Shoes.app do
  background backcolor
 end
```

### ask_open_file() » a string 

Pops up an "Open file..." window. It's the standard window which shows all of your folders and lets you select a file to open. Hands you back the name of the file. 

```ruby
 filename = ask_open_file
 Shoes.app do
   para File.read(filename)
 end
```

You can also specify the title string the dialog displays by including a hash argument  with 'title'. This 'title' feature is new with Shoes 3.3.3 and is also available for ask_save_file, ask_open_folder and ask_save_folder as well as ask_open_file.

```ruby
 Shoes.app do
   button "open special" do
     sel = ask_open_file title: "Shoes Manual wants you to select a file"
     para "Selected: #{sel}"
   end
 end
```

### ask_save_file() » a string 

Pops up a "Save file..." window, similiar to `ask_open_file`, described previously.

```ruby
 save_as = ask_save_file
```

### ask_open_folder() » a string 

Pops up an "Open folder..." window. It's the standard window which shows all of your folders and lets you select a folder to open. Hands you back the name of the folder.

```ruby
 folder = ask_open_folder
 Shoes.app do
   para Dir.entries(folder)
 end
```

### ask_save_folder() » a string 

Pops up a "Save folder..." window, similiar to `ask_open_folder`, described previously. On OS X, this method currently behaves like an alias of `ask_open_folder`.

```ruby
 save_to = ask_save_folder
```


### confirm(question: a string, :title => a string or nil) » true or false 

Pops up a yes-or-no question. If the person at the computer, clicks *'yes*', you'll get back a `true`. If not, you'll get back `false`.

```ruby
 if confirm("Draw a circle?")
  Shoes.app{ oval :top => 0, :left => 0, :radius => 50 }
 end
```
          
`:title` is optional and replace the default "Shoes asks:" with the string you provide or could be set to `nil` in which case the window won't have a title at all.

See [alert](introduction.md#alertmessage-a-string-title-a-string-or-nil-nil) method for similar examples.

### debug(message: a string) » nil 

Sends a debug message to the Shoes console.  You can bring up the Shoes console by pressing `Alt-/` on any Shoes window (or `⌘-/` on OS X.)

```ruby
 debug("Running Shoes on " + RUBY_PLATFORM)
```

Also check out the [error](introduction.md#errormessage-a-string-nil), [warn](introduction.md#warnmessage-a-string-nil) and [info](introduction.md#infomessage-a-string-nil) methods.

### error(message: a string) » nil 

Sends an error message to the Shoes console.  This method should only be used to log errors.  Try the [debug](introduction.md#debugmessage-a-string-nil) method for logging messages to yourself.

Oh, and, rather than a string, you may also hand exceptions directly to this method and they'll be formatted appropriately.

### Shoes.quit() 

Stops your program. Call this anytime you want to suddenly call it quits.
It stops all the graphical things in a graceful way and eventually terminate your program. 

### exit() 

This is no longer recommended as the way to quit a Shoes program. Instead  use Shoes.quit(). Overriding the Ruby exit method has some undesired behaviour  in some situations such as:

*'PLEASE NOTE:*' If you need to use Ruby's own `exit` method (like in a forked Ruby process,) call `Kernel.exit`.

exit it can also confuse the debugger (the code) and you. So Shoes.quit  is better.

### font(message: a string) » an array of font family names 

Loads a TrueType (or other type of font) from a file.  While TrueType is supported by all platforms, your platform may support other types of fonts.

Shoes uses each operating system's built-in font system to make this work.

Here's a rough idea of what fonts work on which platforms:

 * Bitmap fonts (.bdf, .pcf, .snf) - Linux
 * Font resource (.fon) - Windows
 * Windows bitmap font file (.fnt) - Linux, Windows
 * PostScript OpenType font (.otf) - Mac OS X, Linux, Windows
 * Type1 multiple master (.mmm) - Windows
 * Type1 font bits (.pfb) - Linux, Windows
 * Type1 font metrics (.pfm) - Linux, Windows
 * TrueType font (.ttf) - Mac OS X, Linux, Windows
 * TrueType collection (.ttc) - Mac OS X, Linux, Windows

If the font is properly loaded, you'll get back an array of font names found in the file.  Otherwise, `nil` is returned if no fonts were found in the file.

Also of interest: the `Shoes::FONTS` constant is a complete list of fonts available to you on this platform.  You can check for a certain font by using `include?`.

```ruby
 if Shoes::FONTS.include? "Helvetica"
   alert "Helvetica is available on this system."
 else
   alert "You do not have the Helvetica font."
 end
```

If you have trouble with fonts showing up, make sure your app loads the font before it is used.  Especially on OS X, if fonts are used before they are loaded, the font cache will tend to ignore loaded fonts.

### gradient(color1, color2) » Shoes::Pattern 

Builds a linear gradient from two colors.  For each color, you may pass in a Shoes::Color object or a string describing the color.
```ruby
Shoes.app do
   fill gradient(red, green)
   oval 10, 10, 150
end
```

### gray(the numbers: darkness, alpha) » Shoes::Color 

Create a grayscale color from a level of darkness and, optionally, an alpha level.

```ruby
 black = gray(0.0)
 white = gray(1.0)
```

```ruby
Shoes.app do
   fill gray(127)
   oval 10, 10, 150
end
```

### info(message: a string) » nil 

Logs an informational message to the user in the Shoes console.  So, where debug messages are designed to help the program figure out what's happening, `info` messages tell the user extra information about the program.

```ruby
 info("You just ran the info example on Shoes #{Shoes::RELEASE_NAME}.")
```

For example, whenever a Shy file loads, Shoes prints an informational message in the console describing the author of the Shy and its version.

### rgb(a series of numbers: red, green, blue, alpha) » Shoes::Color 

Create a color from red, green and blue components.  An alpha level (indicating transparency) can also be added, optionally.

When passing in a whole number, use values from 0 to 255.

```ruby
 blueviolet = rgb(138, 43, 226)
 darkgreen = rgb(0, 100, 0)
```

Or, use a decimal number from 0.0 to 1.0.

```ruby
 blueviolet = rgb(0.54, 0.17, 0.89)
 darkgreen = rgb(0, 0.4, 0)
```

This method may also be called as `Shoes.rgb`.

### warn(message: a string) » nil 

Logs a warning for the user.  A warning is not a catastrophic error (see [error](introduction.md#errormessage-a-string-nil) for that.)  It is just a notice that the program will be changing in the future or that certain parts of the program aren't reliable yet.

To view warnings and errors, open the Shoes console with `Alt-/` (or `⌘-/` on OS X.)

## The App Object 

An App is a single window running code at a URL. When you switch URLs, a new App object is created and filled up with stacks, flows and other Shoes elements.

The App is the window itself. Which may be closed or cleared and filled with new elements. 

![example](img/introduction_example.png)

The App itself, in slot/box terminology, is a flow.  See the *Slots* section for more, but this just means that any elements placed directly at the top-level will flow.

### Shoes.app(styles) { ... } » Shoes::App 

Starts up a Shoes app window.  This is the starting place for making a Shoes program.  Inside the block, you fill the window with various Shoes elements (buttons, artwork, etc.) and, outside the block, you use the `styles` to
describe how big the window is.  Perhaps also the name of the app or if it's resizable.

```ruby
 Shoes.app(:title => "White Circle",
   :width => 200, :height => 200, :resizable => false) {
     background black
     fill white
     oval :top => 20, :left => 20, :radius => 160
 }
```

In the case above, a small window is built.  200 pixels by 200 pixels.  It's not resizable.  And, inside the window, two elements: a black background and a white circle.

Once an app is created, it is added to the [Apps](introduction.md#the-app-object) list.  If you want an app to spawn more windows, see the [window](slots.md#windowstyles-shoesapp) method and the [dialog](slots.md#dialogstyles-shoesapp) method.

### Shoes.APPS() » An array of Shoes::App objects 

Builds a complete list of all the Shoes apps that are open right now.  Once an app is closed, it is removed from the list.  Yes, you can run many apps at once in Shoes.  It's completely encouraged.

### clipboard() » a string 

Returns a string containing all of the text that's on the system clipboard. This is the global clipboard that every program on the computer cuts and pastes into.

```ruby
Shoes.app(:title => "Copy and Paste", :width => 450, :height => 250) do
   button "copy" do
      self.clipboard = "this is a Shoes string."
   end
   button "paste" do
      para clipboard()
   end
end
```

### clipboard = a string 

Stores `a string` of text in the system clipboard.

```ruby
Shoes.app(:title => "Copy and Paste", :width => 450, :height => 250) do
   button "copy" do
      self.clipboard = "this is a Shoes string."
   end
   button "paste" do
      para clipboard()
   end
end
```

### close() 

Closes the app window.  If multiple windows are open and you want to close the entire application, use the built-in method `exit`.

### download(url: a string, styles) 

Starts a download thread (much like XMLHttpRequest, if you're familiar with JavaScript.)  This method returns immediately and runs the download in the background.  Each download thread also fires `start`, `progress` and `finish` events.  You can send the download to a file or just get back a string (in the `finish` event.)

If you attach a block to a download, it'll get called as the `finish` event.

```ruby
 Shoes.app do
   stack do
     title "Searching Google", :size => 16
     @status = para "One moment..."

     # Search Google for 'shoes' and print the HTTP headers
     download "http://www.google.com/search?q=shoes" do |goog|
       @status.text = "Headers: " + goog.response.headers.inspect
     end
   end
 end
```

And, if we wanted to use the downloaded data, we'd get it using `goog.response.body`.  This example is truly the simplest form of `download`: pulling some web data down into memory and handling it once it's done.

Another simple use of `download` is to save some web data to a file, using the `:save` style.

```ruby
 Shoes.app do
   stack do
     title "Downloading Google image", :size => 16
     @status = para "One moment..."

     download "http://www.google.com/logos/nasa50th.gif",
       :save => "nasa50th.gif" do
         @status.text = "Okay, is downloaded."
     end
   end
 end
```

In this case, you can still get the headers for the downloaded file, but `response.body` will be `nil`, since the data wasn't saved to memory.  You will need to open the file to get the downloaded goods.

If you need to send certain headers or actions to the web server, you can use the `:method`, `:headers` and `:body` styles to customize the HTTP request.
(And, if you need to go beyond these, you can always break out Ruby's OpenURI class.)

```ruby
 Shoes.app do
   stack do
     title "GET Google", :size => 16
     @status = para "One moment..."

     download "http://www.google.com/search?q=shoes", 
         :method => "GET" do |dump|
       @status.text = dump.response.body
     end
   end
 end
```

As you can see from the above example, Shoes makes use of the "GET" method to query google's search engine.

### location() » a string 

Gets a string containing the URL of the current app.

### mouse() » an array of numbers: button, left, top 

Identifies the mouse cursor's location, along with which button is being pressed.

```ruby
 Shoes.app do
   @p = para
   animate do
     button, left, top = self.mouse
     @p.replace "mouse: #{button}, #{left}, #{top}"
   end
 end
```

### owner() » Shoes::App 

Gets the app which launched this app.  In most cases, this will be `nil`.  But if this app was launched using the  [window](slots.md#windowstyles-shoesapp) method, the owner will be the app which called `window`.

### started?() » true or false 

Has the window been fully constructed and displayed?  This is useful for threaded code which may try to use the window before it is completely built.
(Also see the `start` event which fires once the window is open.)

### visit(url: a string) 

Changes the location, in order to view a different Shoes URL.

Absolute URLs (such as http://google.com) are okay, but Shoes will be expecting a Shoes application to be at that address.  (So, google.com won't work, as it's an HTML app.)

## The Styles Master List 

You want to mess with the look of things?  Well, throughout Shoes, styles are used to change the way elements appear.  In some cases, you can even style an entire class of elements.  (Like giving all paragraphs a certain font.)

Styles are easy to spot.  They usually show up when the element is created.

```ruby
 Shoes.app :title => "A Styling Sample" do
   para "Red with an underline", :stroke => red, :underline => "single"
 end
```

Here we've got a `:title` style on the app.  And on the paragraph inside the app, a red `:stroke` style and an `:underline` style.

The style hash can also be changed by using the [style](slots.md#style-styles) method, available on every element and slot.

```ruby
 Shoes.app :title => "A Styling Sample" do
   @text = para "Red with an underline"
   @text.style(:stroke => red, :underline => "single")
 end
```

Most styles can also be set by calling them as methods.  (I'd use the manual search to find the method.)

```ruby
 Shoes.app :title => "A Styling Sample" do
   @text = para "Red with an underline"
   @text.stroke = red
   @text.underline = "single"
 end
```

Rather than making you plow through the whole manual to figure out what styles go where, this helpful page speeds through every style in Shoes and suggests where that style is used.

### :align » a string 

For: *banner, caption, code, del, em, ins, inscription, link, para, span, strong, sub, sup, subtitle, tagline, title*

The alignment of the text. It is either:

 * "left" - Align the text to the left.
 * "center" - Align the text in the center.
 * "right" - Align the text to the right.

### :angle » a number 

For: *background, border, gradient*.

The angle at which to apply a gradient.  Normally, gradient colors range from top to bottom.  If the `:angle` is set to 90, the gradient will rotate 90 degrees counter-clockwise and the gradient will go from left to right.

```ruby
Shoes.app do
   fill gradient(red, green, angle: 35)
   oval 10, 10, 150
end
```

### :attach » a slot or element 

For: *flow, stack*.

Pins a slot relative to another slot or element.  Also, one may write  `:attach #> Window` to position the slot at the window's top, left corner.  Taking this a bit further, the style `:top => 10, :left => 10, :attach => Window` would place the slot at (10, 10) in the window's coordinates.

If a slot is attached to an element that moves, the slot will move with it.  If the attachment is reset to `nil`, the slot will flow in with the other objects that surround, as normal.

### :autoplay » true or false 

For: *video*.

Should this video begin playing after it appears?  If set to `true`, the video will start without asking the user.

### :bottom » a number 

For: *all slots and elements*.

Sets the pixel coordinate of an element's lower edge.  The edge is placed relative to its container's lower edge.  So, `:bottom => 0` will align the element so that its bottom edge and the bottom edge of its slot touch.

### :cap » :curve or :rect or :project 

For: *arc, arrow, border, flow, image, mask, rect, star, shape, stack*.

Sets the shape of the line endpoint, whether curved or square.  See the [cap](slots.md#capcurve-or-rect-or-project-self) method for more explanation.

### :center » true or false 

For: *arc, image, oval, rect, shape*.

Indicates whether the `:top` and `:left` coordinates refer to the center of the shape or not.  If set to `true`, this is similar to setting the [transform](slots.md#transformcenter-or-corner-self) method to `:center`.

### :change » a proc 

For: *edit_box, edit_line, list_box*.

The `change` event handler is stored in this style.  See the [change](elements.md#change-self-self) method for the edit_box, as an example.

### :finish » a proc 

For: *edit_line*.

The `finish` handler is called (if set) when the user presses the Enter or Return key in an edit_line.  See [finish](elements.md#finish-self-self)

### :checked » true or false 

For: *check, radio*.

Is this checkbox or radio button checked?  If set to `true`, the box is checked.  Also see the [checked](elements.md#checked-true-or-false_1) method.

### :choose » a string 

For: *list_box*.

Sets the currently chosen item in the list.  More information at [choose](elements.md#chooseitem-a-string-self).

### :click » a proc 

For: *arc, arrow, banner, button, caption, check, flow, image, inscription, line, link, mask, oval, para, radio, rect, shape, stack, star, subtitle, tagline, title*.

The `click` event handler is stored in this style.  See the [click](slots.md#click-button-left-top-mods-self) method for a description.

### :curve » a number 

For: *background, border, rect*.

The radius of curved corners on each of these rectangular elements.  As an example, if this is set to 6, the corners of the rectangle are given a curve with a 6-pixel radius.

### :dash » a string 

For: *arrow, border, line, oval, rect, shape*.

Defines the line style used to draw elements. It can be either:

 * :onedot - Creates dot-dash line.
 * :nodot - Creates solid line.

If dash style is undefined, ":nodot" is set as default.

```ruby
Shoes.app width: 410, height: 60 do
   line 10,20,400,20, dash: :onedot, stroke: green, strokewidth: 3
   line 10,40,400,40, stroke: green, strokewidth: 3
end
```

### :displace_left » a number 

For: *all slots and elements*.

Moves a shape, text block or any other kind of object to the left or right.  A positive number displaces to the right by the given number of pixels; a negative number displaces to the left.  Displacing an object doesn't effect the actual layout of the page.  Before using this style, be sure to read the [displace](slots.md#displaceleft-a-number-top-a-number-self) docs, since its behavior can be a bit surprising. 

### :displace_top » a number 

For: *all slots and elements*.

Moves a shape, text block or any other kind of object up or down.  A positive number moves the object down by this number of pixels; a negative number moves it up.  Displacing doesn't effect the actual layout of the page or the object's true coordinates.  Read the [displace](slots.md#displaceleft-a-number-top-a-number-self) docs, since its behavior can be a bit surprising.

### :emphasis » a string 

For: *banner, caption, code, del, em, ins, inscription, link, para, span, strong, sub, sup, subtitle, tagline, title*.

Styles the text with an emphasis (commonly italicized.)

This style recognizes three possible settings:

 * "normal" - the font is upright.
 * "oblique" - the font is slanted, but in a roman style.
 * "italic" - the font is slanted in an italic style. 

### :family » a string 

For: *banner, caption, code, del, em, ins, inscription, link, para, span, strong, sub, sup, subtitle, tagline, title*.

Styles the text with a given font family.  The string should contain the family name or a comma-separated list of families.

### :fill » a hex code, a Shoes::Color or a range of either 

For: *arc, arrow, background, banner, caption, code, del, em, flow, image, ins, inscription, line, link, mask, oval, para, rect, shape, span, stack, star, strong, sub, sup, subtitle, tagline, title*.

The color of the background pen.  For shapes, this is the fill color, the paint inside the shape.  For text stuffs, this color is painted in the background (as if marked with a highlighter pen.)

### :font » a string 

For: *banner, caption, code, del, em, ins, inscription, link, para, span, strong, sub, sup, subtitle, tagline, title, list_box*.

Styles the text with a font description.  The string is pretty flexible, but can take the form "[FAMILY-LIST] [STYLE-OPTIONS] [SIZE]", where FAMILY-LIST is
a comma separated list of families optionally terminated by a comma,
STYLE_OPTIONS is a whitespace separated list of words where each WORD describes
one of style, variant, weight, stretch, or gravity, and SIZE is a decimal
number (size in points) or optionally followed by the unit modifier "px" for
absolute size. Any one of the options may be absent. If FAMILY-LIST is absent,
then the default font family (Arial) will be used.

Note: The list_box control will accept a font string when created but it is
not as flexible as textblock based font settings and there are no methods for
getting the current font or changing it after creation.

### :group » a string 

For: *radio*.

Indicates what group a radio button belongs to.  Without this setting, radio
buttons are grouped together with other radio buttons in their immediate slot.
"Grouping" radio buttons doesn't mean they'll be grouped next to each other on
the screen.  It means that only one radio button from the group can be selected
at a time.

By giving this style a string, the radio button will be grouped with other
radio buttons that have the same group name.

### :height » a number 

For: *all slots and elements*.

Sets the pixel height of this object.  If the number is a decimal number, the
height becomes a percentage of its parent's height (with 0.0 being 0% and 1.0
being 100%.)

### :hidden » true or false 

For: *all slots and elements*.

Hides or shows this object.  Any object with `:hidden => true` are not
displayed on the screen.  Neither are its children.

### :inner » a number 

For: *star*.

The size of the inner radius (in pixels.)  The inner radius describes the solid
circle within the star where the points begin to separate.

### :items » an array 

For: *list_box*.

The list of selections in the list box.  See the [list box](elements.md#listbox) method
for an example.

### :justify » true or false 

For: *banner, caption, code, del, em, ins, inscription, link, para, span, strong, sub, sup, subtitle, tagline, title*

Evenly spaces the text horizontally.

### :kerning » a number 

For: *banner, caption, code, del, em, ins, inscription, link, para, span, strong, sub, sup, subtitle, tagline, title*.

Adds to the natural spacing between letters, in pixels.

### :leading » a number 

For: *banner, caption, inscription, para, subtitle, tagline, title*.

Sets the spacing between lines in a text block.  Defaults to 4 pixels.

### :left » a number 

For: *all slots and elements*.

Sets the left coordinate of this object to a specific pixel.  Setting `:left => 10` places the object's left edge ten pixels away from the left edge of the slot containing it.  If this style is left unset (or set to `nil`,) the object will flow in with the other objects surrounding it.

You might also want to give it a :top, if it's acting a bit funny. Sometimes it needs both. :)

### :margin » a number or an array of four numbers 

For: *all slots and elements*.

Margins space an element out from its surroundings.  Each element has a left, top, right, and bottom margin.  If the `:margin` style is set to a single number, the spacing around the element uniformly matches that number.  In other words, if `:margin => 8` is set, all the margins around the element are set to
eight pixels in length.

This style can also be given an array of four numbers in the form `[left, top, right, bottom]`.

### :margin_bottom » a number 

For: *all slots and elements*.

Sets the bottom margin of the element to a specific pixel size.

### :margin_left » a number 

For: *all slots and elements*.

Sets the left margin of the element to a specific pixel size.

### :margin_right » a number 

For: *all slots and elements*.

Sets the right margin of the element to a specific pixel size.

### :margin_top » a number 

For: *all slots and elements*.

Sets the top margin of the element to a specific pixel size.

### :outer » a number 

For: *star*.

Sets the outer radius (half of the *total* width) of the star, in pixels.

### :points » a number 

For: *star*.

How many points does this star have?  A style of `:points => 5` creates a five-pointed star.

### :radius » a number 

For: *arc, arrow, background, border, gradient, oval, rect, shape*.

Sets the radius (half of the diameter or total width) for each of these elements.  Setting this is equivalent to setting both `:width` and `:height` to double this number.

### :right » a number 

For: *all slots and elements*.

Sets the pixel coordinate of an element's right edge.  The edge is placed relative to its container's rightmost edge.  So, `:right => 0` will align the element so that its own right edge and the right edge of its slot touch.
Whereas `:right => 20` will position the right edge of the element off to the left of its slot's right edge by twenty pixels.

### :rise » a number 

For: *banner, caption, code, del, em, ins, inscription, link, para, span, strong, sub, sup, subtitle, tagline, title*.

Lifts or plunges the font baseline for some text. For example, a [sup](slots.md#suptext-shoessup) has a `:rise` of 10 pixels.  Conversely, the [sub](slots.md#subtext-shoessub) element has a `:rise` of -10 pixels.

### :scroll » true or false 

For: *flow, stack, background*.

Establishes this slot as a scrolling slot.  If `:scroll => true` is set, the slot will show a scrollbar if any of its contents go past its height.  The scrollbar will appear and disappear as needed.  It will also appear inside the width of the slot, meaning the slot's width will never change, regardless of
whether there is a scrollbar or not.

When used on a background instead of a flow or stack the background and set true the background will always fill the slot when scrolled. See [background](elements.md#background).

### :secret » true or false 

For: *ask, edit_line*.

Used for password fields, this setting keeps any characters typed in from becoming visible on the screen.  Instead, a replacement character (such as an asterisk) is show for each letter typed.

### :size » a number 

For: *banner, caption, code, del, em, ins, inscription, link, para, span, strong, sub, sup, subtitle, tagline, title*.

Sets the pixel size for the font used inside this text block or text fragment.

Font size may also be augmented, through use of the following strings:

 * "xx-small" - 57% of present size.
 * "x-small" - 64% of present size.
 * "small" - 83% of present size.
 * "medium" - no change in size.
 * "large" - 120% of present size.
 * "x-large" - 143% of present size.
 * "xx-large" - 173% of present size.

### :state » a string 

For: *button, check, edit_box, edit_line, list_box, radio, slider*.

The `:state` style is for disabling or locking certain controls, if you don't want them to be edited.

Here are the possible style settings:

 * nil - the control is active and editable.
 * "readonly" - the control is active but cannot be edited.
 * "disabled" - the control is not active (grayed out) and cannot be edited.

### :stretch » a string 

For: *banner, caption, code, del, em, ins, inscription, link, para, span, strong, sub, sup, subtitle, tagline, title*.

Sets the font stretching used for a text object.

Possible settings are:

 * "condensed" - a smaller width of letters.
 * "normal" - the standard width of letters.
 * "expanded" - a larger width of letters.

### :strikecolor » a Shoes::Color 

For: *banner, caption, code, del, em, ins, inscription, link, para, span, strong, sub, sup, subtitle, tagline, title*.

The color used to paint any lines stricken through this text.

### :strikethrough » a string 

For: *banner, caption, code, del, em, ins, inscription, link, para, span, strong, sub, sup, subtitle, tagline, title*.

Is this text stricken through?  Two options here:

 * "none" - no strikethrough
 * "single" - a single-line strikethrough.

### :stroke » a hex code, a Shoes::Color or a range of either 

For: *arc, arrow, banner, border, caption, code, del, em, flow, image, ins, inscription, line, link, mask, oval, para, rect, shape, span, stack, star, strong, sub, sup, subtitle, tagline, title*.

The color of the foreground pen.  In the case of shapes, this is the color the lines are drawn with.  For paragraphs and other text, the letters are printed in this color.

### :strokewidth » a number 

For: *arc, arrow, border, flow, image, line, mask, oval, rect, shape, star, stack*.

The thickness of the stroke, in pixels, of the line defining each of these shapes.  For example, the number two would set the strokewidth to 2 pixels.

### :text » a string 

For: *button, edit_box, edit_line*.

Sets the message displayed on a button control, or the contents of an edit_box or edit_line.

### :top » a number 

For: *all slots and elements*.

Sets the top coordinate for an object, relative to its parent slot.  If an object is set with `:top => 40`, this means the object's top edge will be placed 40 pixels beneath the top edge of the slot that contains it.  If no
`:top` style is given, the object is automatically placed in the natural flow of its slot.

You should probably give it a :left, too, if it's acting strange. It likes to have both. :)

### :undercolor » a Shoes::Color 

For: *banner, caption, code, del, em, ins, inscription, link, para, span, strong, sub, sup, subtitle, tagline, title*.

The color used to underline text.

### :underline » a string 

For: *banner, caption, code, del, em, ins, inscription, link, para, span, strong, sub, sup, subtitle, tagline, title*.

Dictates the style of underline used in the text.

The choices for this setting are:

 * "none" - no underline at all.
 * "single" - a continuous underline.
 * "double" - two continuous parallel underlines.
 * "low" - a lower underline, beneath the font baseline.  (This is generally recommended only for single characters, particularly when showing keyboard accelerators.)
 * "error" - a wavy underline, usually found indicating a misspelling.

### :variant » a string 

For: *banner, caption, code, del, em, ins, inscription, link, para, span, strong, sub, sup, subtitle, tagline, title*.

Vary the font for a group of text.  Two choices:

 * "normal" - standard font.
 * "smallcaps" - font with the lower case characters replaced by smaller variants of the capital characters. 

### :weight » a string 

For: *banner, caption, code, del, em, ins, inscription, link, para, span, strong, sub, sup, subtitle, tagline, title*.

Set the boldness of the text.  Commonly, this style is set to one of the following strings:

 * "ultralight" - the ultralight weight (= 200)
 * "light" - the light weight (= 300)
 * "normal" - the default weight (= 400)
 * "semibold" - a weight intermediate between normal and bold (= 600)
 * "bold" - the bold weight (= 700)
 * "ultrabold" - the ultrabold weight (= 800)
 * "heavy" - the heavy weight (= 900)

However, you may also pass in the numerical weight directly.

### :width » a number 

For: *all slots and elements*.

Sets the pixel width for the element.  If the number is a decimal, the width is converted to a percentage (with 0.0 being 0% and 1.0 being 100%.)  A width of 100% means the object fills its parent slot.

### :wrap » a string 

For: *banner, caption, code, del, em, ins, inscription, link, para, span, strong, sub, sup, subtitle, tagline, title, list_box*

How should the text wrap when it fills its width? Possible options are:

 * "word" - Break lines at word breaks.
 * "char" - Break lines between characters, thus breaking some words.
 * "trim" - Cut the line off with an ellipsis if it goes too long.
 
```ruby
 Shoes.app do
   str = "Shoes is a tiny graphics toolkit. It's simple and straightforward. Shoes was born to be easy!"
   
   stack left: 100, width: 200 do
      para str, wrap: "word"
      para str, wrap: "char"
      para str, wrap: "trim"
   end
end
```

## Classes List 

Here is a complete list of all the classes introduced by Shoes.  This chart is laid out according to how classes inherits from each other.  Subclasses are indented one level to the right, beneath their parent class.

- Data
    - Shoes::App
        - Shoes::Dialog
    - Shoes::Canvas
        - Shoes
            - Shoes::Contraint
            - Shoes::Flow
            - Shoes::Layout
            - Shoes::Mask
            - Shoes::Stack
            - Shoes::Widget
    - Shoes::Color
    - Shoes::Effect
    - Shoes::Image
    - Shoes::LinkUrl
    - Shoes::Menu
    - Shoes::Menubar
    - Shoes::Menuitem
    - Shoes::Mouse
    - Shoes::Native
        - Shoes::Button
        - Shoes::Check
        - Shoes::EditBox
        - Shoes::EditLine
        - Shoes::ListBox
        - Shoes::Progress
        - Shoes::Radio
        - Shoes::ShoesEvent
        - Shoes::Slider
        - Shoes::Spinner
        - Shoes::Switch
        - Shoes::TextView
    - Shoes::Pattern
        - Shoes::Background
        - Shoes::Border
    - Shoes::Plot
    - Shoes::Settings
    - Shoes::Shape
    - Shoes::Svg
    - Shoes::SvgHandle
    - Shoes::Systray
    - Shoes::Text
        - Shoes::Code
        - Shoes::Del
        - Shoes::Em
        - Shoes::Ins
        - Shoes::link
        - Shoes::LinkHover
        - Shoes::Span
        - Shoes::Strong
        - Shoes::Sub
        - Shoes::Sup
    - Shoes::TextBlock
        - Shoes::Banner
        - Shoes::Caption
        - Shoes::Inscription
        - Shoes::Para
        - Shoes::Subtitle
        - Shoes::Tagline
        - Shoes::Title
    - Shoes::TimerBase
        - Shoes::Animation
        - Shoes::Every
        - Shoes::Timer
    - Shoes::Video
    - Shoes::Windows
- Exception
    - StandardError
        - Shoes::ImageError
        - Shoes::InvalidModeError
        - Shoes::NotImplementedError
        - Shoes::SettingUp
        - Shoes::VideoError
- Shoes::Download
- Shoes::HttpResponse

## Colors List 

The following list of colors can be used throughout Shoes.  As background colors or border colors.  As stroke and fill colors.  Most of these colors come from the X11 and HTML palettes.

All of these colors can be used by name.  (So calling the `tomato` method from inside any slot will get you a nice reddish color.)  Below each color, also find the exact numbers which can be used with the [rgb](introduction.md#rgba-series-of-numbers-red-green-blue-alpha-shoescolor) method.

| Color name | RGB value |
|------------|-----------|
| aliceblue | rgb(240, 248, 255)|
| antiquewhite | rgb(250, 235, 215)|
| aqua | rgb(0, 255, 255)|
| aquamarine | rgb(127, 255, 212)|
| azure | rgb(240, 255, 255)|
| beige | rgb(245, 245, 220)|
| bisque | rgb(255, 228, 196)|
| black | rgb(0, 0, 0)|
| blanchedalmond | rgb(255, 235, 205)|
| blue | rgb(0, 0, 255)|
| blueviolet | rgb(138, 43, 226)|
| brown | rgb(165, 42, 42)|
| burlywood | rgb(222, 184, 135)|
| cadetblue | rgb(95, 158, 160)|
| chartreuse | rgb(127, 255, 0)|
| chocolate | rgb(210, 105, 30)|
| coral | rgb(255, 127, 80)|
| cornflowerblue | rgb(100, 149, 237)|
| cornsilk | rgb(255, 248, 220)|
| crimson | rgb(220, 20, 60)|
| cyan | rgb(0, 255, 255)|
| darkblue | rgb(0, 0, 139)|
| darkcyan | rgb(0, 139, 139)|
| darkgoldenrod | rgb(184, 134, 11)|
| darkgray | rgb(169, 169, 169)|
| darkgreen | rgb(0, 100, 0)|
| darkkhaki | rgb(189, 183, 107)|
| darkmagenta | rgb(139, 0, 139)|
| darkolivegreen | rgb(85, 107, 47)|
| darkorange | rgb(255, 140, 0)|
| darkorchid | rgb(153, 50, 204)|
| darkred | rgb(139, 0, 0)|
| darksalmon | rgb(233, 150, 122)|
| darkseagreen | rgb(143, 188, 143)|
| darkslateblue | rgb(72, 61, 139)|
| darkslategray | rgb(47, 79, 79)|
| darkturquoise | rgb(0, 206, 209)|
| darkviolet | rgb(148, 0, 211)|
| deeppink | rgb(255, 20, 147)|
| deepskyblue | rgb(0, 191, 255)|
| dimgray | rgb(105, 105, 105)|
| dodgerblue | rgb(30, 144, 255)|
| firebrick | rgb(178, 34, 34)|
| floralwhite | rgb(255, 250, 240)|
| forestgreen | rgb(34, 139, 34)|
| fuchsia | rgb(255, 0, 255)|
| gainsboro | rgb(220, 220, 220)|
| ghostwhite | rgb(248, 248, 255)|
| gold | rgb(255, 215, 0)|
| goldenrod | rgb(218, 165, 32)|
| gray | rgb(128, 128, 128)|
| green | rgb(0, 128, 0)|
| greenyellow | rgb(173, 255, 47)|
| honeydew | rgb(240, 255, 240)|
| hotpink | rgb(255, 105, 180)|
| indianred | rgb(205, 92, 92)|
| indigo | rgb(75, 0, 130)|
| ivory | rgb(255, 255, 240)|
| khaki | rgb(240, 230, 140)|
| lavender | rgb(230, 230, 250)|
| lavenderblush | rgb(255, 240, 245)|
| lawngreen | rgb(124, 252, 0)|
| lemonchiffon | rgb(255, 250, 205)|
| lightblue | rgb(173, 216, 230)|
| lightcoral | rgb(240, 128, 128)|
| lightcyan | rgb(224, 255, 255)|
| lightgoldenrodyellow | rgb(250, 250, 210)|
| lightgreen | rgb(144, 238, 144)|
| lightgrey | rgb(211, 211, 211)|
| lightpink | rgb(255, 182, 193)|
| lightsalmon | rgb(255, 160, 122)|
| lightseagreen | rgb(32, 178, 170)|
| lightskyblue | rgb(135, 206, 250)|
| lightslategray | rgb(119, 136, 153)|
| lightsteelblue | rgb(176, 196, 222)|
| lightyellow | rgb(255, 255, 224)|
| lime | rgb(0, 255, 0)|
| limegreen | rgb(50, 205, 50)|
| linen | rgb(250, 240, 230)|
| magenta | rgb(255, 0, 255)|
| maroon | rgb(128, 0, 0)|
| mediumaquamarine | rgb(102, 205, 170)|
| mediumblue | rgb(0, 0, 205)|
| mediumorchid | rgb(186, 85, 211)|
| mediumpurple | rgb(147, 112, 219)|
| mediumseagreen | rgb(60, 179, 113)|
| mediumslateblue | rgb(123, 104, 238)|
| mediumspringgreen | rgb(0, 250, 154)|
| mediumturquoise | rgb(72, 209, 204)|
| mediumvioletred | rgb(199, 21, 133)|
| midnightblue | rgb(25, 25, 112)|
| mintcream | rgb(245, 255, 250)|
| mistyrose | rgb(255, 228, 225)|
| moccasin | rgb(255, 228, 181)|
| navajowhite | rgb(255, 222, 173)|
| navy | rgb(0, 0, 128)|
| oldlace | rgb(253, 245, 230)|
| olive | rgb(128, 128, 0)|
| olivedrab | rgb(107, 142, 35)|
| orange | rgb(255, 165, 0)|
| orangered | rgb(255, 69, 0)|
| orchid | rgb(218, 112, 214)|
| palegoldenrod | rgb(238, 232, 170)|
| palegreen | rgb(152, 251, 152)|
| paleturquoise | rgb(175, 238, 238)|
| palevioletred | rgb(219, 112, 147)|
| papayawhip | rgb(255, 239, 213)|
| peachpuff | rgb(255, 218, 185)|
| peru | rgb(205, 133, 63)|
| pink | rgb(255, 192, 203)|
| plum | rgb(221, 160, 221)|
| powderblue | rgb(176, 224, 230)|
| purple | rgb(128, 0, 128)|
| red | rgb(255, 0, 0)|
| rosybrown | rgb(188, 143, 143)|
| royalblue | rgb(65, 105, 225)|
| saddlebrown | rgb(139, 69, 19)|
| salmon | rgb(250, 128, 114)|
| sandybrown | rgb(244, 164, 96)|
| seagreen | rgb(46, 139, 87)|
| seashell | rgb(255, 245, 238)|
| sienna | rgb(160, 82, 45)|
| silver | rgb(192, 192, 192)|
| skyblue | rgb(135, 206, 235)|
| slateblue | rgb(106, 90, 205)|
| slategray | rgb(112, 128, 144)|
| snow | rgb(255, 250, 250)|
| springgreen | rgb(0, 255, 127)|
| steelblue | rgb(70, 130, 180)|
| tan | rgb(210, 180, 140)|
| teal | rgb(0, 128, 128)|
| thistle | rgb(216, 191, 216)|
| tomato | rgb(255, 99, 71)|
| turquoise | rgb(64, 224, 208)|
| violet | rgb(238, 130, 238)|
| wheat | rgb(245, 222, 179)|
| white | rgb(255, 255, 255)|
| whitesmoke | rgb(245, 245, 245)|
| yellow | rgb(255, 255, 0)|
| yellowgreen | rgb(154, 205, 50)|
