# Chart_series 

This an object that you can explicitly create (or subclass if needed). It describes a group of data (series) to be drawn in a plot.

## chart_series {options} 

Returns a chart_series object you can add to a plot. The options are exactly identical to those you give for plot.add {options}. 

For the advanced explorer, you can do some fun stuff if you work with explict chart_series object(s). Most of the options you set when adding a hash of options can be modifed. Perhaps you have new data to add to the plot widget or you want to  change the color based on the new data arriving or change the min/max . We have getters and setters to do that. 

It's not sub second real time graphing - computers vary but every 1 or 2 second is not unresonable if your willing to  put that load on your box (or your user's boxen). I suggest you stick to the coarse Shoe aninmate or every methods (much slower) before you go wild and deep into Ruby threading. If you ever think you need
to change a threads priority, stop digging that hole deeper and climb out for a rethink.

One other thing to consider: Shoes does not clone or dup or unfreeze arrays or strings. Just reference. If that makes no sense, just know that we told you so and there is nothing Shoes can do about it.

## chart_series.values 

Returns the entire values array. A mistake just waiting for a catalyst.

## chart_series.value = <array> 

Replace the series values with a new array. A mistake just waiting for a catalyst.

## chart_series.labels 

Returns the entire labels array. A mistake just waiting for a catalyst.

## chart_series.labels = <array> 

Replace the series labels with a new array. A mistake just waiting for a catalyst.

## chart_series.at <integer> 

Returns a two element array of [label[index], values[index]], of type [String, Number]

## chart_series.get index 

Same as .at . Returns a two element array of [label[index], values[index]]

## chart_series.set <integer>, ["new string", new_number] 

This has some meat. Chew carefully.  The <integer> could be an existing observation to change or it could be s new observation to append, depending on the <integer> The <integer> is a Ruby  0 based index into both the label and values arrays. You can append (expand the Ruby arrays)

If you have 7 entries (0..6), append at 7 (`array.size`) or face the nil's that plot may not handle.

It will be serious mistake to set `[nil, newvalue]` or `["newlabel", nil]` and expect that Shoes will fix your nil. 
It will segfault, if you are lucky. 
