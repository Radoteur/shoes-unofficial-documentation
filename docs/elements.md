# Elements 

Ah, here's the stuff of Shoes. An element can be as simple as an oval shape. Or
as complex as a video stream. You've encountered all of these elements before
in the Slots section of the manual.

Shoes has seven native controls: the Button, the EditLine, the EditBox, the
ListBox, the Progress meter, the Check box and the Radio.  By "native"
controls, we mean that each of these seven elements is drawn by the operating
system.  So, a Progress bar will look one way on Windows and another way on OS
X.

Shoes also has seven basic other types of elements: Background, Border, Image,
Shape, TextBlock, Timer and Video.  These all should look and act the same on
every operating system.

Once an element is created, you will often still want to change it. To move it
or hide it or get rid of it. You'll use the commands in this section to do that
sort of stuff. (Especially check out the [Common Methods](elements.md#common-methods) section for
commands you can use on any element.)

So, for example, use the `image` method of a Slot to place a PNG on the screen.
The `image` method gives you back an Image object. Use the methods of the Image
object to change things up.

## Common Methods 

A few methods are shared by every little element in Shoes.  Moving, showing,
hiding.  Removing an element.  Basic and very general things.  This list
encompasses those common commands.

One of the most general methods of all is the `style` method (which is also
covered as the [style](slots.md#style-styles) method for slots.)

```ruby
 Shoes.app do
   stack do
     # Background, text and a button: both are elements!
     @back  = background green
     @text  = banner "A Message for You, Rudy"
     @press = button "Stop your messin about!"

     # And so, both can be styled.
     @text.style :size => 12, :stroke => red, :margin => 10
     @press.style :width => 400
     @back.style :height => 10
   end
 end
```

For specific commands, see the other links to the left in the Elements section.
Like if you want to pause or play a video file, check the [video](elements.md#video) section,
since pausing and playing is peculiar to videos.  No sense pausing a button.

### displace(left: a number, top: a number) » self 

Displacing an element moves it.  But without changing the layout around it.
This is great for subtle animations, especially if you want to reserve a place
for an element while it is still animating.  Like maybe a quick button shake or
a slot sliding into view.

When you displace an element, it moves relative to the upper-left corner where
it was placed.  So, if an element is at the coordinates (20, 40) and you
displace it 2 pixels left and 6 pixels on top, you end up with the coordinates
(22, 46).

```ruby
 Shoes.app do
   flow :margin => 12 do
     # Set up three buttons
     button "One"
     @two = button "Two"
     button "Three"

     # Bounce the second button
     animate do |i|
       @two.displace(0, (Math.sin(i) * 6).to_i)
     end
   end
 end
```

Notice that while the second button bounces, the other two buttons stay put.
If we used a normal `move` in this situation, the second button would be moved
out of the layout and the buttons would act as if the second button wasn't
there at all.  (See the [move](elements.md#moveleft-a-number-top-a-number-self) example.)

**Of particular note:** if you use the `left` and `top` methods to get the
coordinates of a displaced element, you'll just get back the normal
coordinates.  As if there was no displacement.  Displacing is just intended for
quick animations!

### height() » a number 

The vertical screen size of the element in pixels. In the case of images, this
is not the full size of the image. This is the height of the element as it is
shown right now.

If you have a 150x150 pixel image and you set the width to 50 pixels, this
method will return 50.

Also see the [width](elements.md#width-a-number) method for an example and some other comments.

### hide() » self 

Hides the element, so that it can't be seen.  See also [show](elements.md#show-self) and
[toggle](elements.md#toggle-self).

### left() » a number 

Gets you the pixel position of the left edge of the element.

### move(left: a number, top: a number) » self  

Moves the element to a specific pixel position within its slot.  The element is
still inside the slot.  But it will no longer be stacked or flowed in with the
other stuff in the slot.  The element will float freely, now absolutely
positioned instead.

```ruby
 Shoes.app do
   flow :margin => 12 do
     # Set up three buttons
     button "One"
     @two = button "Two"
     button "Three"

     # Bounce the second button
     animate do |i|
       @two.move(40, 40 + (Math.sin(i) * 6).to_i)
     end
   end
 end
```

The second button is moved to a specific place, allowing the third button to
slide over into its place.  If you want to move an element without shifting
other pieces, see the [displace](elements.md#displaceleft-a-number-top-a-number-self) method.

### parent() » a Shoes::Stack or Shoes::Flow 

Gets the object for this element's container.  Also see the slot's
[content](slots.md#contents-an-array-of-elements) to do the opposite: get a container's elements.

### remove() » self 

Removes the element from its slot.  (In other words: throws it in the garbage.)
The element will no longer be displayed.

### show() » self 

Reveals the element, if it is hidden.  See also [hide](elements.md#hide-self) and
[toggle](elements.md#toggle-self).

### style() » styles 

Gives you the full set of styles applied to this element, in the form of a
Hash.  While methods like `width` and `height` and `top` give you back specific
pixel dimensions, using `style[:width]` or `style[:top]`, you can get the
original setting (things like "100%" for width or "10px" for top.)

```ruby
 Shoes.app do
   # A button which take up the whole page
   @b = button "All of it", :width => 1.0, :height => 1.0

   # When clicked, show the styles
   @b.click { alert(@b.style.inspect) }
 end
```

### style(styles) » styles 

Changes the style of an element.  This could include the `:width` and `:height`
of an element, the font `:size` of some text, the `:stroke` and `:fill` of a
shape.  Or any other number of style settings.

### toggle() » self 

Hides an element if it is shown.  Or shows the element, if it is hidden.

### tooltip() » a string 

Returns a string containing the tooltip text from an element. 

```ruby
Shoes.app do
   button "push me!", tooltip: "push me at your own risk..." do
      alert("...seriously?")
   end
end
```

### tooltip = a string 

Set the tooltip text from an element with the given string.

```ruby
Shoes.app do
   @e = edit_line
   @b = button "set tooltip" do
      @e.tooltip = @e.text
      @b.tooltip = @e.text
      @b2.tooltip = @e.text
   end
   @b2 = button "get tooltip" do
      @p.text = @b.tooltip
   end
   @p = para
end
```

### top() » a number 

Gets the pixel position of the top edge of the element.

### width() » a number 

Gets the pixel width for the full size of the element.  This method always
returns an exact pixel size.  In the case of images, this is not the full width
of the image, just the size it is shown at.  See the [height](elements.md#height-a-number) method
for more.

Also, if you create an element with a width of 100% and that element is inside
a stack which is 120 pixels wide, you'll get back `120`.  However, if you call
`style[:width]`, you'll get `"100%"`.

```ruby
 Shoes.app do
   stack :width => 120 do
     @b = button "Click me", :width => "100%" do
       alert "button.width = #{@b.width}\n" +
         "button.style[:width] = #{@b.style[:width]}"
     end
   end
 end
```

In order to set the width, you'll have to go through the [style](elements.md#style-styles)
method again.  So, to set the button to 150 pixels wide: `@b.style(:width =>
150)`.

To let Shoes pick the element's width, go with `@b.style(:width => nil)` to
empty out the setting.

## Background 

A background is a color, a gradient or an image that is painted across an
entire slot.  Both backgrounds and borders are a type of Shoes::Pattern.

![black background](img/elements_background.png)

Even though it's called a ''background'', you may still place this element in
front of other elements.  If a background comes after something else painted on
the slot (like a `rect` or an `oval`,) the background will be painted over that
element.

The simplest background is just a plain color background, created with the
[background](elements.md#background) method, such as this black background:

```ruby
 Shoes.app do
   background black
 end
```

A simple background like that paints the entire slot that contains it.  (In
this case, the whole window is painted black.)

You can use styles to cut down the size or move around the background to your liking.

To paint a black background across the top fifty pixels of the window:

```ruby
 Shoes.app do
   background black, :height => 50
 end
```

Or, to paint a fifty pixel column on the right-side of the window:

```ruby
 Shoes.app do
   background black, :width => 50, :right => 50
 end
```

Since Backgrounds are normal elements as well, see also the start of the
[Elements](elements.md) section for all of its other methods.

## Border 

A border is a color, gradient or image painted in a line around the edge of any
slot.  Like the Background element in the last section, a Border is a kind of
Shoes::Pattern.

![borders](img/elements_border.png)

The first, crucial thing to know about border is that all borders paint a line
around the **inside** of a slot, not the outside.  So, if you have a slot
which is fifty pixels wide and you paint a five pixel border on it, that means
there is a fourty pixel wide area inside the slot which is surrounded by the
border.

This also means that if you paint a Border on top of a [background](elements.md#background), the
edges of the background will be painted over by the border.

Here is just such a slot:

```ruby
 Shoes.app do
   stack :width => 50 do
     border black, :strokewidth => 5
     para "=^.^=", :stroke => green
   end
 end
```

If you want to paint a border around the outside of a slot, you'll need to wrap
that slot in another slot.  Then, place the border in the outside slot.

```ruby
 Shoes.app do
   stack :width => 60 do
     border black, :strokewidth => 5
     stack :width => 50 do
       para "=^.^=", :stroke => green
     end
   end
 end
```

In HTML and many other languages, the border is painted on the outside of the
box, thus increasing the overall width of the box.  Shoes was designed with
consistency in mind, so that if you say that a box is fifty pixels wide, it
stays fifty pixels wide regardless of its borders or margins or anything else.

Please also check out the [Elements](elements.md) section for other methods used on borders.

## Button 

Buttons are, you know, push buttons.  You click them and they do something.
Buttons are known to say "OK" or "Are you sure?"  And, then, if you're sure,
you click the button. 

![button](img/elements_button.png)

```ruby
 Shoes.app do
   button "OK!"
   button "Are you sure?"
 end
```

The buttons in the example above don't do anything when you click them. In
order to get them to work, you've got to hook up a block to each button.

```ruby
 Shoes.app do
   button "OK!" do
     append { para "Well okay then." }
   end
   button "Are you sure?" do
     append { para "Your confidence is inspiring." }
   end
 end
```

So now we've got blocks for the buttons. Each block appends a new paragraph to
the page. The more you click, the more paragraphs get added.

It doesn't go much deeper than that. A button is just a clickable phrase.

Just to be pedantic, though, here's another way to write that last example.

```ruby
 Shoes.app do
   @b1 = button "OK!"
   @b1.click { para "Well okay then." }
   @b2 = button "Are you sure?"
   @b2.click { para "Your confidence is inspiring." }
 end
```

This looks dramatically different, but it does the same thing.  The first
difference: rather than attaching the block directly to the button, the block
is attached later, through the `click` method.

The second change isn't related to buttons at all.  The `append` block was
dropped since Shoes allows you to add new elements directly to the slot.  So we
can just call `para` directly.  (This isn't the case with the `prepend`,
`before` or `after` methods.)

Beside the methods below, buttons also inherit all of the methods that are
[Common](elements.md#common-methods) but not all of them work. See styles below.

### click() { |self| ... } » self 

When a button is clicked, its `click` block is called.  The block is handed
`self`.  Meaning: the button which was clicked.

### focus() » self 

Moves focus to the button.  The button will be highlighted and, if the user
hits Enter, the button will be clicked.

### button styles 

Shoes 3.3.6 added support for changing the font, font stroke color, tiny icons

```ruby
 Shoes.app do
   flow do
     button "Button", font: "Menlo Bold 14",width: 200, stroke: red,
          tooltip: "Menlo Bold 14" do
     end
     button "icon", width: 80, height: 30, icon: "#{DIR}/static/icon-info.png",
          tooltip: "default right", stroke: blue do
     end
   end
 end
```

`Do not` set height: - It will not work and it won't look right on OSX and arguably does't
look right when it does work for you. Note that the tiny icon is in `addition` to the string. It
is not a button made from an image - that you do for your self with Image and a click {}. 

The icon should fit inside the button - it `will not` be downsized to fit.
You have a few options for placing the itty bitty icon releative to the title string,
left, right. If no title is provided and no position is given, then the icon in centered.

```ruby
 Shoes.app do
   flow do
      button "left", width: 80, icon: "#{DIR}/static/icon-info.png",
          icon_pos: "left", tooltip: "title left" do
      end
      button  width: 80, icon: "#{DIR}/static/icon-info.png",
          tooltip: "just icon" do
      end
      button "right", width: 80, icon: "#{DIR}/static/icon-info.png",
          icon_pos: "right", tooltip: "title right" do
      end
    end
  end
```

Warning! If you speculate that there is a top and a bottom icon_pos you are also
speculating that you can change the height of the button and I've told before that
OSX won't do that so you shouldn't either.  Unsupported to the max. 

## Check 

Check boxes are clickable square boxes than can be either checked or unchecked.
A single checkbox usually asks a "yes" or "no" question.  Sets of checkboxes
are also seen in to-do lists.

![checkbox](img/elements_checkbox.png)

Here's a sample checklist.

```ruby
 Shoes.app do
   stack do
     flow { check; para "Frances Johnson" }
     flow { check; para "Ignatius J. Reilly" }
     flow { check; para "Winston Niles Rumfoord" }
   end
 end
```

You basically have two ways to use a check.  You can attach a block to the
check and it'll get called when the check gets clicked.  And/or you can just
use the `checked?` method to go back and see if a box has been checked or not.

Okay, let's add to the above example.

```ruby
 Shoes.app do
   @list = ['Frances Johnson', 'Ignatius J. Reilly',
     'Winston Niles Rumfoord']

   stack do
     @list.map! do |name|
       flow { @c = check; para name }
       [@c, name]
     end

     button "What's been checked?" do
       selected = @list.map { |c, name| name if c.checked? }.compact
       alert("You selected: " + selected.join(', '))
     end
   end
 end
```

So, when the button gets pressed, each of the checks gets asked for its status,
using the `checked?` method.

You can also set a default `checked` value.

```ruby
Shoes.app do
   flow { check; para "Default check (unchecked)" }
   flow { check checked: false; para "Unchecked check" }
   flow { check checked: true; para "Checked check" }
end
```

Button methods are listed below, but also see the list of [Common methods](elements.md#common-methods)),
which all elements respond to.

### checked?() » true or false 

Returns whether the box is checked or not.  So, `true` means "yes, the box is checked!"

### checked = true or false 

Marks or unmarks the check box.  Using `checked = false`, for instance,
unchecks the box.

### click() { |self| ... } » self 

When the check is clicked, its `click` block is called.  The block is handed
`self`, which is the check object which was clicked.

Clicks are sent for both checking and unchecking the box.

### focus() » self 

Moves focus to the check.  The check will be highlighted and, if the user hits
Enter, the check will be toggled between its checked and unchecked states.

## EditBox 

Edit boxes are wide, rectangular boxes for entering text.  On the web, they
call these textareas.  These are multi-line edit boxes for entering longer
descriptions.  Essays, even! 

![editbox](img/elements_editbox.png)

Without any other styling, edit boxes are sized 200 pixels by 108 pixels.  You
can also use `:width` and `:height` styles to set specific sizes.

```ruby
 Shoes.app do
   edit_box
   edit_box :width => 100, :height => 100
 end
```

Other controls (like [button](elements.md#button) and [check](elements.md#check) have only click events, but both
[EditLine](elements.md#editline) and [EditBox](elements.md#editbox) have a `change` event.  The `change` block is called
every time someone types into or deletes from the box.

```ruby
 Shoes.app do
   edit_box do |e|
     @counter.text = e.text.size
   end
   @counter = strong("0")
   para @counter, " characters"
 end
```

Notice that the example also uses the [text](elements.md#text-self_1) method inside the block.
That method gives you a string of all the characters typed into the box.

More edit box methods are listed below, but also see the list of [Common methods](elements.md#common-methods)), which all elements respond to.

### change() { |self| ... } » self 

Each time a character is added to or removed from the edit box, its `change`
block is called. The block is given `self`, which is the edit box object which
has changed.

### focus() » self 

Moves focus to the edit box. The edit box will be highlighted and the user will
be able to type into the edit box.

### text() » self 

Return a string of characters which have been typed into the box.

### text = a string 

Fills the edit box with the characters of `a string`.

### append = a string 

Appends `a string` to the end of the current contents of the edit_box. 
If you need a newline then you'll have to add it to `a string`

This method appeared in Shoes 3.2.25 and may not be available in Shoes 4.

### scroll_to_end 

Scrolls the end_box so the last line is showing. If you are appending strings
to the end and want to show them when they scroll out of view, then you
would want to use this method.

```ruby
 Shoes.app height: 400, width: 640 do
  stack do
    @tb = edit_box "foo", width: 0.8
    @tb.text = "Not your everyday edit_box\n"
    cnt = 0
    button "append" do
      cnt = cnt + 1
      @tb.append("append line #{cnt}\n")
      @tb.scroll_to_end
      #puts "text is: #{@tb.text}"
    end
    button "check" do
      confirm @tb.text
    end
  end
end
```

This method appeared in Shoes 3.2.25 and may not be available in Shoes 4.

### styles 

Shoes 3.3.6 allows you to set the font and stroke but you may find cross platform
problems when you do that. Not really a good thing to do so don't depended on it and YMMV
(your milage may vary). This works fine on OSX and not so well on Linux and Windows:

```ruby
Shoes.app height: 500, width: 300 do
  stack do
    flow do
      para "Normal "
       @eb1 = edit_box "Normal", tooltip: "normal"
    end
    flow do 
      para "Font  "
      @eb2 = edit_box "Font Is?", font: "Monaco 9", tooltip: "monaco 9"
    end
    flow do
      para "Stroke "
      @eb3 = edit_box "Stroke", stroke: red, tooltip: "red"
    end
    flow do
      para "Both   "
      @eb4 = edit_box "Font and Stroke", font: "Monaco 14", stroke: green,
        tooltip: "14pt fixed & green"
    end
    flow do
      button "Append to" do
        @eb1.append "\nAppended"
        @eb2.append "\nAppended"
        @eb3.append "\nAppended"
        @eb4.append "\nAppended"
      end
      button "change all" do
        @eb1.text = "I've been reset1"
        @eb2.text = "I've been reset1"
        @eb3.text = "I've been reset1"
        @eb4.text = "I've been reset1"
      end
    end
  end
end
```

## EditLine 

Edit lines are a slender, little box for entering text. While the EditBox is
multi-line, an edit line is just one. Line, that is. Horizontal, in fact.

![editline](img/elements_editline.png)

The unstyled edit line is 200 pixels wide and 28 pixels wide. Roughly. The
height may vary on some platforms.

```ruby
 Shoes.app do
   stack do
     edit_line
     edit_line :width => 400
   end
 end
```

You can change the size by styling both the `:width` and the `:height`.
However, you generally only want to style the `:width`, as the height will be
sized to fit the font. (And, in current versions of Shoes, the font for edit
lines and edit boxes cannot be altered anyway.)

If a block is given to an edit line, it receives `change` events. Check out the
[EditBox](elements.md#editbox) page for an example of using a change block. In fact, the edit box
has all the same methods as an edit line. Also see the list of [Common methods](elements.md#common-methods)), which all elements respond to.

### change() { |self| ... } » self 

Each time a character is added to or removed from the edit line, its `change`
block is called. The block is given `self`, which is the edit line object which
has changed. 

### focus() » self 

Moves focus to the edit line. The edit line will be highlighted and the user
will be able to type into the edit line.

### finish() { |self| ...} » self 

This is called when the enter key (or return key) is pressed in an edit line control
It might be useful if you want a search box when something has to happen when
the enter key is pressed.  Think of it as a 'click' on a hidden button.

It's separate from 'change' event: 

```ruby
 Shoes.app do
  stack do
    @el = edit_line do |e|
      para e.text+"\n"
    end
    @el.finish = proc { |slf| para "enterkey #{slf.text}\n" }
  end
 end
```


This only works on Shoes 3.2 and is not in Shoes 4.

### text() » self 

Return a string of characters which have been typed into the box.

### text = a string 

Fills the edit line with the characters of `a string`.

### styles 

Shoes 3.3.6 added the ability to set the font and stroke color. There are
many good reasons `not to do this` if you care about cross platform compatibilty.
It `does not` play nice with any themes your user may have chosen. It won't work
everywhere....

```ruby
Shoes.app height: 300, width: 300 do
  stack do
    flow do
      para "Normal "
       @el1 = edit_line "Normal", tooltip: "Normal"
    end
    flow do 
      para "Font  "
      @el2 = edit_line "Font Is?", font: "Monaco 9", tooltip: "monaco 9"
    end
    flow do
      para "Stroke "
      @el3 = edit_line "Stroke", stroke: red, tooltip: "red"
    end
    flow do
      para "Both"
      @el4 = edit_line "Stroke and Font", stroke: green, font: "Arial 14",
       tooltip: "green arial 14"
    end
    button "change contents" do
      @el1.text = "Changed!"
      @el2.text = "Changed!"
      @el3.text = "Changed!"
      @el4.text = "Changed!"
    end
  end
end
```

## Image 

An image is a picture in PNG, JPEG or GIF format. Shoes can resize images or
flow them in with text. Images can be loaded from a file or directly off the
web. 

![image](img/elements_image.png)

To create an image, use the `image` method in a slot:

```ruby
 Shoes.app do
   para "Nice, nice, very nice.  Busy, busy, busy."
   image "#{DIR}/static/shoes-manual-apps.png"
  end
```

When you load any image into Shoes, it is cached in memory. This means that if
you load up many image elements from the same file, it'll only really load the
file once.

You can use web URLs directly as well.

```ruby
 Shoes.app do
   image "http://shoesrb.com/img/shoes-icon.png"
 end
```

When an image is loaded from the web, it's cached on the hard drive as well as
in memory. This prevents a repeat download unless the image has changed. (In
case you're wondering: Shoes keeps track of modification times and etags just
like a browser would.)

Sometimes, you don't want Shoes to cache images. For just one particular image
just add add  'cache: false' 

```ruby
 Shoes.app do
   image "http://shoesrb.com/img/shoes-icon.png", cache: false
 end
```

You can also disable all image caching - see [AppMethods](advanced_app_methods.md#appmethods)

Shoes may load remote images in the background using system threads. So,
using remote images may not block Ruby or any intense graphical displays you
may have going on. May not. Assume you'll wait.

### full_height() » a number 

The full pixel height of the image. Normally, you can just use the
[height](elements.md#height-a-number) method to figure out how many pixels high the image is. But
if you've resized the image or styled it to be larger or something, then
`height` will return the scaled size.

The `full_height` method gives you the height of image (in pixels) as it was
stored in the original file.

### full_width() » a number 

The full pixel width of the image. See the [full_height](elements.md#full_height-a-number) method for an
explanation of why you might use this method rather than [width](elements.md#width-a-number).

### path() » a string 

The URL or file name of the image.

### path = a string 

Swaps the image with a different one, loaded from a file or URL.
 
### rotate(degrees: a number) » self 

See [rotate](slots.md#rotatedegrees-a-number-self) on Shapes for details. 
Note that unike Shapes one could call the method also directly on Images.

```ruby
Shoes.app do
    img = image "#{DIR}/static/man-ele-image.png", left: 70, top: 60
    img.rotate 45
end
```

### scale(sx: a float, sy: a float) » self 

See [scale](slots.md#scalesx-a-float-sy-a-float-self) on Shapes for details. 
Note that unike Shapes one could call the method also directly on Images.

```ruby
Shoes.app do
    img = image "#{DIR}/static/man-ele-image.png", left: 70, top: 60
    img.scale 1.2, 0.8
end
```

### skew(sx: a float, sy: a float) » self 

See [skew](slots.md#skewsx-a-float-sy-a-float-self) on Shapes for details. 
Note that unike Shapes one could call the method also directly on Images.

```ruby
Shoes.app do
    img = image "#{DIR}/static/man-ele-image.png", left: 70, top: 60
    img.skew 7.5, 9.5
end
```


 Image effects on image blocks

You already know how to combine shapes into an image block (See [Image and Shape Blocks](introduction.md#image-and-shape-blocks), using the same technique you can add some effects to those image blocks, namely : blur, shadow and glow, with the help of corresponding methods.
Just wrap shapes or images into an image block and add effects inside the block 

```ruby
Shoes.app :width => 400, :height => 450, :resizable => true do
    image :top => 20, :left => 20 do
        fill "#127"
        nostroke
        oval 80, 40, 200, 50
        blur 3
        glow 10, fill: rgb(0.8, 0.4, 0, 0.9)
        glow 10, inner: true, fill: rgb(0.8, 0.4, 0, 0.9)
        shadow radius: 20, fill: rgb(0, 0, 0, 0.75), displace_left: -20, displace_top: 30
    end
    
    image :width => 700, :height => 500, :top => 180, :left => 20 do
        image "#{DIR}/static/app-icon.png", :top => 40, :left => 60
        glow 10, inner: true, fill: yellow
        shadow radius: 10, fill: rgb(0, 0, 0, 0.75), displace_left: -10, displace_top: 10
    end
end
```

Pay attention to some rules :

 * Effects are using the alpha channel of your image if any.
 * Shadow effect must be the last one applied to be predictable.
 * For effects to work correctly you can mix shapes with shapes and images with images but not shapes with images !
 * If you experience some artifacts or glitches, that's certainly because your effect doesn't have enough "room" to work correctly ! Try to make your image block bigger and/or move your inside shape/image towards the center of the image block (the idea is to make room for your effect to expand nicely), ultimately you can also decrease the "strength" of the effect (meaning decrease the radius attribute value).

### blur(radius: a number)  
Blurs the image by `radius` amount.

### glow(radius: a number, :inner  false or true)  
Adds a glow to your image, either outer glow (default) or inner glow (set optional `:inner` style to true), amount of glow is set by `radius` attribute.

Note that you can control the color of the effect with the `:fill` style and the transparency of the effect with the alpha component of the color, ie : rgb(red, green, blue, **alpha**).

### shadow(distance: a number, radius: a number)  
Adds a shadow to your image, `distance` sets the placement of the shadow along a line going from top-left corner to bottom-right corner, the biggest the attribute the more distant the shadow, blurriness of the shadow is controlled by the `radius` attribute.

If you need more precise placement, the method offers two styles for that : `:displace_left` and `:displace_top`, if used they take precedence over the `distance` attribute.

Note that you can control the color of the effect with the `:fill` style and the transparency of the effect with the alpha component of the color, ie : rgb(red, green, blue, **alpha**).

## ListBox 

List boxes (also called "combo boxes" or "drop-down boxes" or "select boxes" in
some places) are a list of options that drop down when you click on the box.

![listbox](img/elements_listbox.png)

A list box gets its options from an array.  An array (a list) of strings,
passed into the `:items` style.

```ruby
 Shoes.app do
   para "Choose a fruit:"
   list_box :items => ["Grapes", "Pears", "Apricots"]
 end
```

So, the basic size of a list box is about 200 pixels wide and 28 pixels high.
You can adjust this length using the `:width` style.

```ruby
 Shoes.app do
   para "Choose a fruit:"
   list_box :items => ["Grapes", "Pears", "Apricots"],
     :width => 120, :choose => "Apricots" do |list|
       @fruit.text = list.text
   end

   @fruit = para "No fruit selected"
 end
```

Next to the `:width` style, the example uses another useful option. The
`:choose` option tells the list box which of the items should be highlighted
from the beginning. (There's also a [choose](elements.md#chooseitem-a-string-self) method for highlighting
an item after the box is created.)

List boxes also have a [change](elements.md#change-self-self_2) event. In the last example, we've got
a block hooked up to the list box. Well, okay, see, that's a `change` block.
The block is called each time someone changes the selected item.

Those are the basics. Might you also be persuaded to look at the [Common methods](elements.md#common-methods)) page, a complete list of the methods that all elements have?

There is also a much reguested option introduced in Shoes 3.3.0 that
allows you to specify the font to be used when you create the list_box.
The string you give is not as flexible as the textblock options for font handling
mention in Styles.  Experiment. For example a simple font: "6" does work for setting the 
size of the default font. 

List_box will also accept the wrap style. 

Note: the font and wrap styles does nothing on OSX because OSX handles long list_box
items much better than Linux or Windows. 

```ruby
 Shoes.app :width => 400, :height => 100 do
 stack do
   items = []
   items[0]= "No! Don't tell me"
   items[1]= "Let me tell you a story about how octal numbers fell out of \
favor and were replaced with hexidecimal numbers."
   list_box :items => items, :choose => items[1], font: "Sans Bold 6"
  end
 end
```


### change() { |self| ... } » self 

Whenever someone highlights a new option in the list box (by clicking on an
item, for instance,) its `change` block is called. The block is given `self`,
which is the list box object which has changed.

### choose(item: a string) » self 

Selects the option in the list box that matches the string given by `item`.

### focus() » self 

Moves focus to the list box. The list box will be highlighted and, if the user
hits the up and down arrow keys, other options in the list will be selected.

### items() » an array of strings 

Returns the complete list of strings that the list box presently shows as its options.

### items = an array of strings 

Replaces the list box's options with a new list of strings.

**Caution!** You can manipulate Ruby Arrays in many ways that Shoes **will not** detect
and Shoes will not display the updated array. For example '<<' will not update
what Shoes displays. += will work because it just happens to be like 
items = . 

When changing the list box from a Shoes program, always copy the
items to a new array, manipulated that and then set the items = new_array

### text() » a string 

A string containing whatever text is shown highlighted in the list box right
now. If nothing is selected, `nil` will be the reply.

### styles 

Shoes 3.3.6 adds the ability to set the font and stroke color. `Beware!` This is
highly platform dependent - it doesn't work `at all` with OSX and probably never will.

```ruby
Shoes.app height: 200, width: 300 do
  stack do
    flow do
      para "Normal "
      list_box :items => ["one", "two", "three"], choose: "two", tooltip: 
        "normal combo"
    end
    flow do 
      para "Font  "
      list_box items: ["Alpha", "Bravo", "Charlie"], font: "Monaco 9",
        choose: "Bravo", tooltip: "monaco 9 font"
    end
    flow do
      para "Stroke "
      list_box items: ["Ask", "your", "mother!"], choose: "your", stroke: red,
        tooltip: "stroke red"
    end
  end
end
```

## Progress 

Progress bars show you how far along you are in an activity. Usually, a
progress bar represents a percentage (from 0% to 100%.) Shoes thinks of
progress in terms of the decimal numbers 0.0 to 1.0. 

![progress](img/elements_progress.png)

A simple progress bar is 200 pixels wide, but you can use the `:width` style
(as with all Shoes elements) to lengthen it.

```ruby
 Shoes.app do
   stack :margin => 0.1 do
     title "Progress example"
     @p = progress :width => 1.0

     animate do |i|
       @p.fraction = (i % 100) / 100.0
     end
   end
 end
```

Take a look at the [Common methods](elements.md#common-methods)) page for a list of methods found an all
elements, including progress bars.

### fraction() » a decimal number 

Returns a decimal number from 0.0 to 1.0, indicating how far along the progress bar is.

### fraction = a decimal number 

Sets the progress to a decimal number between 0.0 and 1.0.

## Radio 

Radio buttons are a group of clickable circles. Click a circle and it'll be
marked. Only one radio button can be marked at a time. (This is similar to the
ListBox, where only one option can be selected at a time.) 

![radio](img/elements_radio.png)

So, how do you decide when to use radio buttons and when to use list boxes?
Well, list boxes only show one highlighted item unless you click on the box and
the drop-down appears. But radio buttons are all shown, regardless of which is
marked.

Radios have been rewritten and the rules clarified: You want to use the group 
arguement for radios. Group names are unique to the App (a window) and not the slot 
as previously versions of Shoes tried (and failed). Radios without a group name
act like checkboxes and may not be unselectable. You really want to use a group name!
Further, radios in a group do not have a default button selected (particularly on osx) so 
you should always designate one of them as 'checked'


Here is three radios w/o a group name.
```ruby
 Shoes.app do
   stack do
     para "Among these films, which do you prefer?"
     flow { radio; para "The Taste of Tea by Katsuhito Ishii" }
     flow { radio; para "Kin-Dza-Dza by Georgi Danelia" }
     flow { radio; para "Children of Heaven by Majid Majidi" }
   end
 end
```

This can be fixed, though. You can group together radios from different slots,
you just have to give them all the same group name.

Here, let's group all these radios in the `:films` group. Note how the 
second item is set to checked.

```ruby
 Shoes.app do
   stack do
     para "Among these films, which do you prefer?"
     flow do
       radio :films
       para "The Taste of Tea by Katsuhito Ishii"
     end
     flow do
       radio :films, checked: true
       para "Kin-Dza-Dza by Georgi Danelia"
     end
     flow do
       radio :films
       para "Children of Heaven by Majid Majidi"
     end
   end
 end
```

For more methods beyond those listed below, also look into the [Common methods](elements.md#common-methods)) page. Because you get those methods on every radio as well.

### checked?() » true or false 

Returns whether the radio button is checked or not. So, `true` means "yes, it
is checked!"

### checked = true or false 

Marks or unmarks the radio button. Using `checked = false`, for instance,
clears the radio.

### click() { |self| ... } » self 

When the radio button is clicked, its `click` block is called. The block is
handed `self`, which is an object representing the radio which was clicked.

Clicks are sent for both marking and unmarking the radio.

### focus() » self 

Moves focus to the radio. The radio will be highlighted and, if the user hits
Enter, the radio will be toggled between its marked and unmarked states.

## Shape 

A shape is a path outline usually created by drawing methods like `oval` and
`rect`. 

![shape](img/elements_shape.png)

All art and shape are explained [here](slots.md#art-for-slots).

See the [Common section](elements.md#common-methods).  Shapes respond to all of those methods.

## Slider 

Sliders allows you to adjust a value, from 0.0 to 1.0, thanks to the handle that can be moved with the mouse or by using the arrow keys. 

![slider](img/elements_slider.png)

here's a sample showing you a slider in action :

```ruby
Shoes.app do
    stack margin: 10 do
        flow do
            @sl = slider fraction: 0.35, state: nil do |sd|
                @p.text = "value : #{sd.fraction}"
            end
            @p = para "", margin_left: 10
        end
        button "switch slider state" do
            @sl.state = (@sl.state == "disabled" ? nil : "disabled")
        end
    end
end 
```

For more methods beyond those listed below, also look into the [Common methods](elements.md#common-methods)) page. Because you get those methods on every slider as well.

### fraction() » a decimal number 

Returns a decimal number from 0.0 to 1.0, depending on how far along the slider bar we are.

### fraction = a decimal number 

Sets the slider to a decimal number between 0.0 and 1.0.

### state » a string 

See [state](introduction.md#state-a-string) style for details.

### state = nil, "disabled" or "readonly" 

See [state](introduction.md#state-a-string) style for details.

## Spinner 

A Spinner is a widget that allows you display the "I'm busy" rotating 
animated wheel-of-slow

![spinner](img/elements_spinner.png)

Here is a sample spinner:
```ruby
Shoes.app(width: 200, height: 80) do
   stack left: 60, top: 20 do
      spinner width: 80, start: true, tooltip: "waiting for something?"
   end
end
```

Spinner methods are listed below, but also see the list of [Common methods](elements.md#common-methods)),
which all elements spinner may respond to. The default for a spinner is too
not aninamte (spin). 

Caution: In OSX, this is not a native control so it won't have a tooltip

### started? » true or false 

Returns either true or false whether the `spinner` element is started or stopped.

```ruby
Shoes.app do
   @s = spinner width: 80
   
   click do |btn, left, top|
      @s.started? ? @s.stop : @s.start
      @p.text = @s.started?
   end
   
   @p = para
end
```

### start() » self 

Start a `spinner` element.

### stop() » self 

Stop a `spinner` element.

### tooltip() » a string 

Returns a string containing the tooltip text from a `spinner` element.

```ruby
Shoes.app do
   spinner tooltip: "Not spinning enough?"
end
```

### tooltip = a string 

Set the tooltip text from a `spinner` element with the given string.

```ruby
Shoes.app(:width => 450, :height => 250) do
   @s = spinner start: true
   
   every(1) do |count|
      @s.tooltip = "count #{count}"
   end
end
```

## Svg 

In Shoes 3.3 we gained the ability to draw svgs. While they may appear to
look like Images, and they respond to many methods that Images respond to, 
they are not the same. It's a different widget with its own rules, some of
them may be quite surprising.

Svg's are xml text files that contain vector grapic drawing instructions.
They don't really have heights and widths. They can stretch or shrink in
any direction. They also have a feature they call 'groups' An Svg file can contain
sub images in it. The best example is a deck of playing cards.  'paris.svg' 
We include the card deck 'paris.svg' in the Shoes samples directory. 
Without a group name it's a large picture of all cards. Each card in the svg file
has a group name. 

Before you can make use of an svg file. You need to learn what's in it
and what you want to do with it (in total or by group). Shoes can help you
much with that task. Inkscape is a cross platform, free application that
you can use to view, change and create svg's. It's not for beginners but
it's the best you can find for free. 

In order to get Svgs working in Shoes and be Shoes compatible you have to
specify the height and width (and the source ) There two samples in the good
section of the furthermore->samples that you should study good-svgview and good-flip


### svg  source {width: integer, height: integer, options} 

You have to specify the width and height you want. There is no default.
Those are the size of the SVG widget on screen in pixels

`source` is a file pathname to the svg or a string containing the svg xml or
an svghandle. Svghandle is a bit weird for the casual Shoes user (see [Svghandle](advanced_svghandle.md#svghandle))

```ruby
Shoes.app do
   svg "#{DIR}/samples/paris.svg"
end
```

The hash has some other `options`:

`:group => string`

This how you pick one group from the larger svg to display The string starts with '#'  
so in a svg of playing cards for example you might want to draw '#diamond_queen'
That is the group name. Shoes can not figure out the group names for you. Sorry. 
If you don't specify a ':group' then Shoes draws everything in the svg which may
or may not be what you want.

```ruby
Shoes.app do
   svg "#{DIR}/samples/paris.svg", group: "#diamond_queen"
end
```

`:aspect => boolean or float`

The aspect may be "true" or "false".  The default is "true"
and that will keep the aspect ratio of the svg (group) even if that means
the image width or height is scaled down to fit the widget size. "false" just fills the rectangle
for the svg widget (stretching width and height as needed) 

Aspect can also be a floating point number.  Advanced users.

Any left over white space after computing the aspect will be added be on the right 
or bottom side. Generally, you should know before you create an svg what size
you want it to be and accept the aspect default. Then you'll have no excess 
white space to think about.

Svg has many of the methods of Image or other controls aka widgets. These 
include `remove, move, displace, hide, show, toggle, hidden?, click, release,
hover, leave, parent, top, left, width, and height`.

Below are the new or diffent methods for Svg, and we mention methods that
exist that you should not use or use with extreme caution.

### style(styles) » styles 

Changes the style of an svg.  This could include the `:width` and `:height`
of an svg or any other number of style settings.

Warning: this may return a copy of the xml in the `content:` style which is not 
something you want to do. Don't try to set `content:` this way. 

### preferred_width » a number 

returns the width of the internal svg. This can be more or less than what width
returns. You probably don't want to use this method.

### preferred_height » a number 

returns the height of the internal svg. This can be more or less than what height
returns. You probably don't want to use this method.

### offset_x » a number 

For experts working with groups. returns the group x offset.

### offset_y » a number 

For experts working with groups. returns the group y offset.

### group? string » boolean 

return true if the string is a svg group or false (nil) if not. Remember that
you have to have a '#' in the front of the string

### transform(:center or :corner) » self 

Behaves like [transform](slots.md#transformcenter-or-corner-self).

### translate(left, top) » self 

Behaves like [translate](slots.md#translateleft-top-self)

### rotate(degrees: a number) » self 

Behaves like [rotate](slots.md#rotatedegrees-a-number-self).

### scale(sx: a float, sy: a float) » self 

Behaves like [scale](slots.md#scalesx-a-float-sy-a-float-self)

### skew(sx: a float, sy: a float) » self 

Behaves like [Art.skew]

### handle » svghandle 

All Svg widgets have an internal svghandle associated with them, whether
you explicity created one or not. This returns it.

### handle = svghandle » 

Replaces the widget contents (on screen drawing) with the new svghandle drawing. 
In some situations this can be much faster than removing an svg widget and
creating a new svg widget.  Advanced users.

### dpi » a number 

returns the dots per inch of the svg. Expect a constant value of 72.0 or 75.0 no matter
what. Probably useless. 

###  export {filename: "...", dpi: 90, canvas: true||false}  

This creates a raster (png only) file for the filename: . The `dpi:` setting
is optional (and useless at this time). The `canvas:` setting controls
what's written. If `canvas: true`, then the on screen image is written as
is seen which includes any transparent space on the bottom or right. If the
`canvas: false` is chosen (it's the default) then only the visible part of
the svg image is written. 

`Note` the filename `is not parsed` for the extention to determine if there
is a .png at the end

### save {filename: "...", format: "svg"|"ps"|"pdf", canvas: true||false| 

This saves the onscreen svg to the vector formats of ps, pdf, or svg as specified in the :format 
setting in the hash. The filename `is not parsed` for the extention to determine
what format to use when creating the file.

If `canvas: true`, then the on screen image is written as
is seen which includes any transparent space on the bottom or right. If the
`canvas: false` is chosen (it's the default) then only the visible part of
the svg image is written.

`Note:` When creating an svg file from a svg group this will can create an
svg file that is quite small (or quite larger) which may not pass muster for
strict svg parsing. 

## Switch 

Switch are clickable buttons that can be either on or off. They are fantastic
for your configuration panel.

![switch](img/elements_switch.png)

Here is a sample switch:
```ruby
Shoes.app do
   switch; para "turn on and off"
end
```

Switch methods are listed below, but also see the list of [Common methods](elements.md#common-methods)),
which all elements respond to.

### active? » true or false 

Returns either true or false whether the `switch` element is turned on or off.

```ruby
Shoes.app do
   switch.click do |n|
      @p.text = n.active?
   end
   @p = para
end
```

### active = true or false 

Turn on or off a `switch` element.

```ruby
Shoes.app do
   @s = switch
   
   every(1) do
      @s.active = @s.active? ^ true
   end
end
```

### click() { |self| ... } » self 

When the switch is clicked, its `click` block is called.  The block is handed
`self`, which is the switch object that was clicked.

```ruby
Shoes.app do
   switch.click do |n|
      alert "switch box clicked, active? #{n.active?}"
   end
end
```

Clicks are sent for both switching on and off.

### tooltip() » a string 

Returns a string containing the tooltip text from a `switch` element.

```ruby
Shoes.app do
   switch tooltip: "Turn the switch on or off" do |n|
      @p.text = n.tooltip
   end
   @p = para
end
```

### tooltip = a string 

Set the tooltip text from a `switch` element with the given string.

```ruby
Shoes.app(:title => "Switching On and Off", :width => 450, :height => 250) do
   switch do |n|
      n.tooltip = "Switch is active? #{n.active?}"
   end
end
```

### font styling 

With Shoes 3.3.6, switch can also change fonts and the font color (stroke)

```ruby
Shoes.app do
   flow do
      switch font: "monospace Italic 16", stroke: red; para
   end
   
   flow do
      @n = switch(active: true) do
         @p.text = (@n.active? ? "true": "false") unless @p.nil?
      end
      @p = para
   end
   
   flow do
      @m = switch width: 80
      @m.click do
        #$stderr.puts "Click"
        @m.active? ? @e.start : @e.stop
      end
      @e = every(1) { |count| @q.text = count unless @q.nil? & @m.active? }
      @q = para ""
   end
   
   start do
      @e.stop
      @p.text = @n.active? ? "true" : "false"
      @m.active = false
   end
end
```

## Snapshot 

Shoes can draw an image block into a file instead of on the screen by calling
the snapshot method. The file can be svg, ps, or pdf. 

It's also possible to see it on the screen in certain situations 
You'll have to decide if thats a bug or feature.

### snapshot(:format => a type of image, :filename => a path) {...} » snapshot raw data 

:format could be a svg (default), pdf or ps (it's also the extension of your saved snapshot).

:filename is the path to the file where you save your snapshot (defaults to same filename in same directory as your script file with the choosen extension).

Inside the block you build the image/snapshot you want to end up with :

```ruby
Shoes.app width: 400, height: 400 do 
    ext = "svg"
    ext = "pdf"
    path = "#{LIB_DIR}/snapshotfile.#{ext}"
     
    stack do    
        button("shoot!") do
            r = snapshot :format => ext.to_sym, :filename => path  do
                stroke blue
                strokewidth 4
                fill black
                oval 100, 100, 200
            end
            info r.inspect
            alert path, title: "snapshot saved to :"
            Shoes.show_log
            para "Done" # comment this out and the svg does not show
        end
    end
end 
```

note that the method returns the drawing code (text) of the snapshot, so 
you might take further control about details of your svg image for example.

## Systray 

Systray allows you to send an icon and a text message to the desktop manager
which will be shown where the platform decides it should be (systray for Windows),
Notification area for Linux and OSX. Typically, you would call it for some
long running process, like a email client that got a new message and you the desktop
to know about it.

This is not a widget you can modify. There are no methods other than creating it
and that will return nil (even when it works). There is no guarantee it will work,
particularly if you call it too often and expect everything to show up. That's up to
the OS and Desktop manager, not Shoes. 

You can call it in two ways, with positional arguments or with a hash of arguments.


### systray <string>, <string>, <string> » nil 

The first string is the title, the second is the message to display and the
third string is a file path to the icon. The example below describes the preferred
way

### systray title: <string>, message: <string>, icon: <file path> » nil 

You can also use a hash of arguments.  The example below is worth some study.
```ruby
Shoes.app do
  stack do
    para "Press button and look in your system's notification area"
    ctr = 0;
    button "Notify" do
      ctr += 1
      icp = ''
      if ctr % 3 != 0
        icp = "#{DIR}/static/shoes-icon.png"
      else
        icp = "#{DIR}/static/shoes-icon-red.png"
      end
      systray title: "Shoes Notify", message: "message ##{ctr}", icon: icp
    end
  end 
end
```

Notice how we change the message: and every third click, the icon changes.
Changes to the title: may not appear, depending on your platform so you
not change change it. All three arguments must be included. 

## TextBlock 

The TextBlock object represents a group of text organized as a single element.
A paragraph containing bolded text, for example. A caption containing links and
bolded text. (So, a `caption` is a TextBlock type.  However, `link` and
`strong` are TextClass types.) 

![textblock](img/elements_textblock.png)

All of the various types of TextBlock are found on the [Element Creation](slots.md#element-creation) page.

 * [banner](slots.md#bannertext-shoesbanner), a 48 pixel font.
 * [title](slots.md#titletext-shoestitle), a 34 pixel font.
 * [subtitle](slots.md#subtitletext-shoessubtitle), a 26 pixel font.
 * [tagline](slots.md#taglinetext-shoestagline), an 18 pixel font.
 * [caption](slots.md#captiontext-shoescaption), a 14 pixel font.
 * [para](slots.md#paratext-shoespara), a 12 pixel font.
 * [inscription](slots.md#inscriptiontext-shoesinscription), a 10 pixel font.

### contents() » an array of elements

Lists all of the strings and styled text objects inside this block.

### replace(a string) 

Replaces the text of the entire block with the characters of `a string`.

### text() » a string 

Return a string of all of the characters in this text box. This will strip off
any style or text classes and just return the actual characters, as if seen on
the screen.

### text = a string 

Replaces the text of the entire block with the characters of `a string`.

### to_s() » a string 

An alias for [text](elements.md#text-a-string_3). Returns a flattened string of all of this
TextBlock's contents.

## Timers 

Shoes contains three timer classes: the Animation class, the Every class and
the Timer class. Both Animations and Everies loop over and over after they
start.  Timers happen once. A one-shot timer.

Animations and Everies are basically the same thing. The difference is that
Animations usually happen many, many times per second. And Everies happen only
once every few seconds or rarely.

### start() » self 

Both types of timers automatically start themselves, so there's no need to use
this normally. But if you [stop](elements.md#stop-self_1) a timer and would like to start it up
again, then by all means: use this!

### stop() » self 

Pauses the animation or timer. In the case of a one-shot timer that's already
happened, it's already stopped and this method will have no effect.

### toggle() » self 

If the animation or timer is stopped, it is started. Otherwise, if it is
already running, it is stopped.

## Video 

Shoes 3.3.1 restores the old Video widget capabilies. But differently for
those who remember the old ways: You can't package your app with the VLC libraries included. 
There is no separate video/non-video version of Shoes. There are no buttons
or links created for you. 

What Shoes does is to look for VLC installed on the running system and if its a recent
enough version of VLC (2.1, 2,2) then Shoes will use it. This has implications for
folks that write scripts for other people:  Those end users will have to install
VLC from videolan.org, and they should install it in the default place that
VLC suggests, because thats where Shoes will look.  If you are just write Shoes code
for yourself you also need to install VLC, just like anybody else.

Shoes looks for VLC in `/usr/lib` (for linux), `/Applications/VLC.app` (osx) and
`C:\"Program Files (x86)"\VideoLAN\VLC` which is where VLC installs it's self.

You also have to draw your own buttons or links and hook them up so they 
call the methods to pause, play, fast forward and so on. Take a look at the
expert-video-player in Furthermore->Samples which is tour-de-force of how to create a 
custom widget and wire up. We might get an easier method built in the future but 
for now, you need to do it the hard way.

To add a video you need to

```ruby
require 'shoes/videoffi'
```

**Vlc.load_lib**

This checks that VLC is installed and makes it's api's available to Shoes.  
This call to **load_lib** method is happening transparently when you create a new widget, you don't need it in your scripts.

If Shoes can't find VLC on it's own, and you know you have installed it, you can help Shoes : just go to the Cobbler (you'll find him by clicking on **Maintain Shoes** button at the start screen) once in Cobbler's atelier, click on **VLC setup button** ...). 

**video(path/url: a string, optional styles) » video-widget** 

This creates the on screen widget with the styles args like width and height and some unique settings. This is the widget you control with the methods below.

"path/url" is the path to a file or an address of a stream as a String (everything Vlc can read on your hard drive or over the internet, you're pretty much spoilt for choice !), this parameter is mandatory, note that you can pass "" i.e. an empty string, if you want to start with an empty media player and later load a media with path= method (see below).

There are some styles that are unique to Video widgets:

 * `:autoplay` => true or false

Start playing given media immediately and every time you load a new media. You can change this later by means of autoplay= method (see below). Defaults to `false`.

 * `:volume` => integer 0..100

Defaults to `85`.

 * `:bg_color` => a Shoes color

Sets the background of the video area. Defaults to `black`, rgb(0,0,0).

 * `:vlc_options` => an array of String

**Advanced users !** Vlc player options, if you really need to customize Vlc ! - you're on your own here, lookup for **libvlc_new** arguments -.

One option is worth mentionning, though : "--play-and-pause"
It is related to the **play_list_add** method (see below).
Note that, as mentionned in Vlc doc, there is no guaranty about cross-platform implementation or even between vlc release.

**audio(path/url: a string, optional styles) » video-widget**

Just a convenience method, hiding away the widget. If you need sounds only in your app and don't want a visible widget on screen


### autoplay() » true or false 

Checks if Vlc is in autoplay mode.

### autoplay = true or false 
 
Sets Vlc autoplay mode.

### have_audio_track() » true or nil 

Checks if media have an audio track.

### have_video_track() » true or nil 

Checks if media have a video track.

### hide() » self 

Hides the video. If already playing, the video will continue to play. This just
turns off display of the video. One possible use of this method is to collapse
the video area when it is playing an audio file, such as an MP3.

### loaded() » true or nil 

If you want to check whether Vlc has loaded or not your media.

### length() » a number 

The full length of the video in milliseconds. Returns nil if the video is not
yet loaded.

### move(left, top) » self 

Moves the video to specific coordinates, the (left, top) being the upper left
hand corner of the video.

### next_media » true or false 

When a playlist has been loaded, changes the actual media to the next item in the playlist. Returns true upon success, false if there isn't a next item.

### path() » a String 

Returns the path or url of the media actually loaded, empty string if no media have been yet loaded.

### path= a String 

Loads a new media into your player, could be a file : video, audio, playlist, image or a stream on the web: from popular video sites, radio, etc... Whatever your Vlc could digest.

### pause() 

Pauses the video, if it is playing. To resume playing, call that method again - not the play() method, it will restart from the beginning -.

### playing?() » true of false 

Returns true if the video is currently playing. Or, false if the video is
paused or stopped.

### play() » int 

Starts playing the video, if it isn't already playing. If already playing, the
video is restarted from the beginning.

### play_list_add(path : a String) » true of false 

Adds a new media into the play list. Shoes uses a play list internally, usually there is only one media in it, the one you loaded, but if you want to build a play list dynamically, this is the method you are going to use.

It can be usefull, coupled with the :vlc_options "--play-and-pause", if you want to prepare some media to be played at some point later.
By default a play list keep on playing one media after the other without stopping at the end of one media, that option can change this.
Create a video widget with "--play-and-pause" option, add medias to the play list, use **play_at** method to play the desired media at the specified index in the play list.

for example, say you create a game and want some sounds to be played at some events :
```ruby
SOUNDS = ["path/to/media1", "path/to/media2", "path/to/media3"]
player = audio '', autoplay: false, vlc_options: ["--play-and-pause"]
SOUNDS.each { |snd| player.play_list_add(snd) }

player.play_at 1 # player.play_at 2 or 0 depending on the event
```

### play_at(idx : an Integer) » true of false 

Plays the media at index idx in the play list. See **play_list_add**.

### position() » a decimal 

The position of the video as a decimanl number (a Float) between the beginning
(0.0) and the end (1.0). For instance, a Float value of 0.5 indicates the
halfway point of the video.

### position = a decimal 

Sets the position of the video using a Float value. To move the video to its
25% position: `@video.position = 0.25`.

### previous_media » true or false 

When a playlist has been loaded, changes the actual media to the previous item in the playlist. Returns true upon success, false if there isn't a previous item.

### remove() » self 

Removes the video from its slot. This will stop the video as well.

### show() » self 

Reveals the video, if it has been hidden by the `hide()` method.

### stop() » self 

Stops the video, if it is playing.

### time() » a number 

The time position of the video in milliseconds. So, if the video is 10 seconds
into play, this method would return the number 10000.

### time = a number 

Set the position of the video to a time in milliseconds.

### toggle() » self 

Toggles the visibility of the video. If the video can be seen, then `hide` is
called. Otherwise, `show` is called.

### video_track_height() » an number 

If loaded media has a video track, fetches it's real height. This is not necessarily the height of the widget, because most of the times you gave some dimensions to your widget and Vlc will accomodate for them.

### video_track_width() » an number 

If loaded media has a video track, fetches it's real width. See video_track_height() above.

## Plot 

In Shoes 3.3.2+ you can draw some simple plots or charts or graphs. Line charts,
column charts, scatter charts, pie charts and a variation of Line charts for time series.

A plot widget is a rectangular space on the screen you can place in in a stack
or flow just like any other element. You'll have to specify the height and width in pixels and
then you pass in a hash of options that we'll discuss. Consider this example:

```ruby
Shoes.app width: 800, height: 500 do
  stack do
    para "Plot Demo Line and Column"
    widget_width = 400
    widget_height = 300
    stack do
      flow do
        @grf = plot widget_width, widget_height, title: "My Graph", caption: 
          "Look at that! Booyah!!" , font: "Helvetica", auto_grid: true,
          default: "skip", background: honeydew
         @grf2 = plot widget_width, widget_height+100, title: "Column Graph", caption: 
          "Amazing!!" , font: "Mono", auto_grid: false, 
          default: "skip", background: cornsilk, chart: "column", boundary_box: false
      end
    end
  end
end
```

Click the Run this button and continue reading.

That example doesn't draw any data points! Correct-o-mundo! We have to add them and we'll get to how to
do that soon but there are a bunch of options you want to consider. The most important option is
that chart: <string> and  know that not all options make sense for all chart types. Look closely and you'll notice
that the title string is displayed inside the plot widget. So is the caption string. And there is too much empty
space at the bottom of the widget. 

To draw data inside the plot widget we add another hash to the plot. In this example we
are going to draw two sets of data points and labels into two different chart_types.

```ruby
Shoes.app width: 800, height: 500 do
  @values1 = [24, 22, 10, 13, 20, 8, 22]
  @x_axis1 = ['a','b','c','d','e','f', 'g']
  @values2 = [200, 150, 75, 125, 75, 225, 125] 
  @x_axis2 = ['a','b','c','d','e','f', 'g']
  stack do
    para "Plot Demo Line and Column"
    widget_width = 400
    widget_height = 300
    stack do
      flow do
        @grf = plot widget_width, widget_height, title: "My Graph", caption: 
          "Look at that! Booyah!!" , font: "Helvetica", auto_grid: true,
          default: "skip", background: honeydew
         @grf2 = plot widget_width, widget_height+100, title: "Column Graph", caption: 
          "Amazing!!" , font: "Mono", auto_grid: false, 
          default: "skip", background: cornsilk, chart: "column", boundary_box: false
      end
    end
    @grf.add values: @values1, labels: @x_axis1,
      name: "foobar", min: 6, max: 26 , desc: "foobar Yy", color: "dodgerblue",
       points: true
    @grf.add values: @values2, labels: @x_axis2,
       name: "Tab", min: @values2.min, max: @values2.max, desc: "BarTab", color: "coral",
       points: true, strokewidth: 2

    @grf2.add values: @values1, labels: @x_axis1,
       name: "Bar", min: 0, max:  30, desc: "foobar Yy", color: "crimson",
       points: true, strokewidth: 12
    cs2 = chart_series values: @values2, labels: @x_axis2,
       name: "Tab", min: 50, max: 230, desc: "BarTab", color: "green",
       points: true, strokewidth: 6
    @grf2.add cs2
  end
end
```

If you look closely, you'll notice most of those add's are the same except the second
add to the column graph creates a chart_series first and then adds that to plot widget.
instead of a hash.

[Chart_series](advanced_charts.md#chart_series) is documented in the Advanced Section of the manual. If you prefer to add a hash
of options, Shoes will create a chart_series for you. We'll discuss the options in this section
with more advanced things in the [Chart_series](advanced_charts.md#chart_series) section.

Now you know how it all comes together in some kind of complicated dance of options. Some options belong
to a plot and some options belong to one set of data in the plot (aka the chart_series).

### plot width, height, {options} 

Creates a plot widget of width, height using the options given. The data inside that rectangle
is auto-sized, auto-scaled to fit as best it can in that rectangle. 

### chart: <string> 

String is one of "line", "column", "scatter", "pie", "radar" or "timeseries" This is the type of
chart you want for this plot widget. Timeseries is like "line" except it has extra options
for interacting with very large data sets with hundreds or thousands of data points.

Not all options are used by all chart types. 

### font: <string> 

This is the Shoes font name to be used for all text in this plot widget.
You have no control over which point size is used where. Default is "Helvitica" and
if you happen to pick some font that only exists on your machine, Shoes may default
to something you don't want on another machine or OS without that font.

### title: <string> 

The string will be drawn, centered, at the top of plot in the chosen font. The point size and style
will be 16 bold.

### caption: <string> 

The string will drawn at the bottom of the plot widget as 12 point normal. Do 
not attempt to draw multiple lines captions with a newline in the string. Multiple line 
captions (and titles) won't work so don't do it. 

### background: <color> 

color is something in the [Colors The Colors list](introduction.md#colors-list) or the result of calling 
rgb(). The background is for the entire widget space and some plot types may not respect the setting.

### boundary_box: <boolean> 

If true then a box is drawn around the inner data area.  Not all chart types
will respect the setting (Pie chart for one won't- doesn't hurt if specified, it just won't
be drawn.

### auto_grid: <boolean> 

This another option that is only respected by certain chart types where it makes sense.
(Line, scatter, timeseries and column). 
Draws vertical and horizontal lines at what Shoes thinks would be best for the
data given. 

### default: <string> 

This controls how the plot will deal with nils in your data arrays or labels arrays that you 
add to to the plot. The default is "missing", but some chart types will respect "max" and "min"
so a missing value (nil) is set to the maximum or minimum of their range. Usually, this is not
visually helpful and can be confusing or misleading.  If you need this option you should clean up your data or find 
a more competent chart application and not use Shoes for more than your exploratory work. Just Saying"
"A man has got to now his limitations".

### click: proc {|btn, l, t| } 

Only (barely) useful and only for TimeSeries charts. If the user clicks in the content area of the
plot, Shoes will call the proc with the button number and the top, left values of the mouse scaled to be in the content
area. Figuring out what to do with that info is very complicated.

### column_settings: <array> 

This only used for radar charts and is mandatory. It also requires a long description
and an example.
```ruby
# Radar graph - 
Shoes.app width: 620, height: 480 do
  @pre_test =  [71, 35, 62, 55, 88, 76, 55] 
  @practice =  [70, 53, 83, 94, 71, 59, 82]
  @post_test = [94, 93, 96, 89, 96, 88, 93]
  @dimensions = [ ["Anger", 0, 100, "%3.0f%%"], 
               ["Contempt", 0, 100, "%3.0f%%"],
               {label: "Disgust", min: 0, max: 100, format: "%3.0f%%"},
               ["Fear", 0, 100, "%3.0f%%"],
               ["Joy", 0, 100, "%3.0f%%"],
               ["Sadness", 0, 100, "%3.0f%%"],
               ["Surprise", 0, 100, "%3.0f%%"]
             ]
  stack do
    para "Plot Radar Demo 7"
    flow do 
      button "quit" do Shoes.quit end
    end
    widget_width = 600
    widget_height = 400
    stack do
      flow do
        @grf = plot widget_width, widget_height, title: "Microexpressions Scores", 
          font: "Helvetica", auto_grid: true, grid_lines: 3, label_radius: 1.10,
          default: "skip", chart: "radar", column_settings: @dimensions
      end
    end
    @grf.add values: @pre_test, color: blue, 
      name: "Pre-Test Score", min: 0, max: 100, strokewidth: 3
    cs = app.chart_series values: @practice, color: red,
      name: "Practice Score", min: 0, max: 100, strokewidth: 3
    @grf.add cs
    @grf.add values: @post_test, color: orange,
      name: "Post-Test Score", min: 0, max: 100, strokewidth: 3
  end
end
```

Run the example and study the code.  For the other chart types, Shoes
processes chart_series like a row in speadsheet. Radar needs column info and
column settings: is how you specify it with an array containing  another array or a hash.
If you look at the hash in the example you'll see old friends like label: min: and max:
format: is not something you may have seen before. It's a Ruby printf control string
that will be given a Ruby double to turn into text.  format: is optional but it
defaults to "4.2f" - four digits on the left of the decimal point and 2 digits to 
the right. 

### grid_lines: <integer> 

This is only used for radar charts and is optional. It can be true, false or a number.
The default is true. When true,  Shoes makes it's estimate of how many grid_lines or 'rings'
to draw around before it becomes too busy.  You can override that. False or 0
means none, true or 1 means guess. 3 would mean 3 'rings' and so on. 

### label_radius: <float_number> 

This is only used for radar charts and is optional. The labels in the column_setting
are drawn on outside of the circle at the radius * 1.15 (default). This good
for some charts and not so good for others. 1.15 means 15% outside. 1.20 is probably
are high as you can get away with before colliding into the title string. 0.5 would
would move them into the circle - probably not what you want.

### plot.save_as <filename> 

This method will draw the plot to an .svg, .pdf ,ps, or .png depending on the extention 
of the filename

### plot.zoom (begin, end)  

This method only works on Timeseries plots (think about thousand of data points).
Remember that you can draw up to 6 data sets (chart_series) in some plot types. 
You can set the beginning and ending indicies (integers) for display. This and the click:
proc and some keypress handling will allow you to zoom in, zoom out, and shift left or right.
It does not change the data. It only affects which part of your data is drawn.

### plot.redraw_to(index) 

Timeseries and line plots can be appended to with new data (perhaps you're collecting data
from a sensor or a remote website or some sort of progress starus from a long running process)

This is a plot method!! If your have two or three data sets displayed in one plot widget then
it's up to you to update all of them before calling redraw_to and if your zooming in and appending 
you'll have to figure it out because redraw_to will reset the plot begin to 0 and the end index to the given index which
really should be a new data point.


### plot.add {options} or chart_series. 

create s a chart_series for you from the hash arguments. The chart_series
is added to the plot widget. You can only have one series for a Pie chart. 
You must have two and only two for a Scatter graph. Line, timeseries, and bar charts
can have up to 6, but you probably don't want that many in one widget.

The first add to a plot widget controls what is shown on the horizontal x axis. 

Not all chart types respect all chart settings. 

### plot.add labels: <array> 

The labels: option is mostly required.  It must be an array of strings. In a few
cases Shoes can create one ["1", "2" ....] but you should not depend on that and you 
don't want that.

### plot.add values: <array> 

The values: array must be Ruby numbers and too be safe, they should be positive numbers.
values: and labels: are assummed to have same number of array elements.

### plot.add min: <number> 

This controls y axis auto scaling lower tick for this data. Currently, you want this
to be a positive number. This is a required hash arg for most chart types. 
Although you can calulate it with <array>.min you'll usually set your own for visual reasons.

### plot.add max: <number> 

This controls y axis auto scaling upper tick for this data. Currently, you want this
to be a positive number. This is a required hash arg for most chart types. 
Although you can calulate it with <array>.max you'll usually set your own for visual reasons.

### plot.add name: <string> 

This is a string you can use for what ever makes sense to you. There are methods
to look it up in the plot wdith to get the chart_series.  Required but mostly not all that important to
you. 

### plot.add desc: <string> 

This is the string that will show up in the chart legend. Highly recommended.
If you don't provide it, Shoes will use what ever your name: is. 

### plot.add color: <color> 

This is color to use to draw this data. It can be one to the built in Shoes colors
or an rgb() color you created. Note: Pie charts ignore this and use there own set of
colors. The first entry for a Scatter Chart uses this for the widget and ignores any color:
in the second series of a scatter.

### plot.add strokewidth: <integer> 

This is the width of the line, and/or size of the point. For column charts, it's the width
of the bar. Ignored for Pie charts

### plot.add points: <int, string or boolean> 

Only working for scatter charts. Line and Timeseries don't use this (known bug)
and pie & column don't need it. 

Controls what kind of point to draw. I highly recommend you do not specify this and just
accept that ever Shoes thinks. There is a relationship with strokewidth: which probably make
no sense to the beginner.  Even so, here's how it should work.

Each point has a number inside Shoes - 0 or nil or false means don't draw any point markers. 
That can vanishs your drawing in a Scatter and others. Don't use this unless you're feeling extra
clever. 

The default (which is what you want) is true or 1, You get the default point for your chart tyoe. 
Did I mention this is what you want? A Timeseries charts with 1000's of data points will not disply
the point type until you zoom in close enough that Shoes thins it has enough visual space for them to
be useful.

The string can be "dot", "circle", "box", and "rect" and perhaps drawn according to
strokewidth: - or not.  depending on chart type.  "dot" is a filled in "circle". box is a filled in
"rect"

Have we mentioned that the default is what you want?
